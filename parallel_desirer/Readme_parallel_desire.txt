Python 3 script to run desire in parallel
Beta version
August 20th, 2018

--------------------------------------------------------------------------------
Necessary python packages (if not available they can be easily installed by PIP or Anaconda):
	os
	sys
	numpy
	matplotlib
	time
	astropy
	time
	subprocess
	datetime
	multiprocessing
	ctypes
	smtplib

-------------------------------------------------------------------------------
File/folder description:
	"parallel_desirer.py"- Main script
	"initiallization.txt"- Input parametres file. It contains the information regarding the number of cores, wavelengths, instruments, etc...
	"run_files"- folder containing all necessary input files

--------------------------------------------------------------------------------
To execute run in python 3 envyroment: "python parallel_desirer.py"

--------------------------------------------------------------------------------
Output files:
	"per_ori.fits"- Observed profiles in SIR format
	"inv_res_pre.fits"- Fitted profiles in SIR format
	"inv_res_mod.fits"- Fitted atmospheric model in SIR format
	"res_desirer.pdf"- Pdf file containing the intensity observed, fitted maps, and the velocity and magnetic field maps at different log tau
