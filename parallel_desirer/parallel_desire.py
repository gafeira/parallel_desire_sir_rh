from __future__ import print_function
import sys
import os
script_folder=os.path.dirname(os.path.realpath(sys.argv[0]))
os.chdir(script_folder)
import numpy as np
np.seterr(divide='ignore', invalid='ignore')
import matplotlib.pyplot as plt
import time
import astropy.io.fits as fits
from subprocess import call
import subprocess
import datetime
import multiprocessing
import ctypes
import smtplib
from os.path import expanduser
from scipy.stats import chisquare
import glob
import warnings
from matplotlib.backends.backend_pdf import PdfPages
from scipy.interpolate import interp2d
import copy
warnings.filterwarnings("ignore")
plt.switch_backend('agg')
plt.ioff()
print('_______________________________________________________')
print('_______________________________________________________')
print('_______________________________________________________')
print('Reading data and initializing arrays...')









#Define all functions

#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------

def save_fits(array,file_name):
    '''
    Save array to a fits file.

    Parameters
    ----------
    first: array that you want to save
    second : Name of the fits file. You can add the path before name
    '''
    hdu = fits.PrimaryHDU(np.float32(array))
    hdul = fits.HDUList([hdu])
    hdul.writeto(file_name,overwrite=True)





def make_grid_file_one(file_lo,ll,ws,mw,mmw,l_core_pos,instindex,wheigth_line):
    """
    Create .grid file for SIR

    Parameters
    ----------
    first : number of lines
    second : line index based on the standart LINE file excluding blends
    third : array with min wavelenght
    fourth: array with wavelenght step
    fifth: array with max wavelenght
    """
    file_grid=open(file_lo+'wave.grid','w')
    file_grid.write('IMPORTANT: a) All items must be separated by commas.\n')
    file_grid.write('           b) The first six characters of the last line\n')
    file_grid.write('              in the header (if any) must contain the symbol ---\n')
    file_grid.write('\n')
    file_grid.write('Line and blends indices   :   Initial lambda     Step     Final lambda\n')
    file_grid.write('(in this order)                    (mA)          (mA)         (mA)\n')
    file_grid.write('-----------------------------------------------------------------------\n')
    for k in range(len(ll)):
        if instindex == 1:
            file_grid.write(str(ll[k])+'                      :       '+'{:>4.3f}'.format((mw[k]-l_core_pos[k])*stepwave[k])+',      '+str(ws[k])+',        '+'{:>4.3f}'.format((mmw[k]-l_core_pos[k])*stepwave[k])+'\n')
    file_grid.close()



def make_grid_file_two(file_lo,ll,wheigth_line):
    """
    Create .grid file for SIR

    Parameters
    ----------
    first : number of lines
    second : line index based on the standart LINE file excluding blends
    third : array with min wavelenght
    fourth: array with wavelenght step
    fifth: array with max wavelenght
    """
    file_grid=open(file_lo+'wave.grid','w')
    file_grid.write('IMPORTANT: a) All items must be separated by commas.\n')
    file_grid.write('           b) The first six characters of the last line\n')
    file_grid.write('              in the header (if any) must contain the symbol ---\n')
    file_grid.write('\n')
    file_grid.write('Line and blends indices   :   Initial lambda     Step     Final lambda\n')
    file_grid.write('(in this order)                    (mA)          (mA)         (mA)\n')
    file_grid.write('-----------------------------------------------------------------------\n')
    for k in range(len(ll)):
        file_grid.write(str(ll[k])+'                      :       '+'{:>4.3f}'.format(wave_leng[k][0][0])+',      '+str(wave_leng[k][0][-1]-wave_leng[k][0][-2])+',        '+'{:>4.3f}'.format(wave_leng[k][0][-1])+'\n')
    file_grid.close()




def convert_profile_one(ll,instindex,core_pos,inwave,fiwave,stepwave,data,x,y,wave_leng):
    '''
    Create the .per files

    Parameters
    ----------
    first: array that you want to save
    second : Name of the fits file. You can add the path before name
    '''
    fil=open('profiles.per','w')
    for k in range(len(ll)):
        wheigth_line=wave_leng[k]
        line_indx_fin=ll[k].split(',')[0]
        inst=instindex
        l_core_pos=core_pos[k]
        prof_data=data[k]
        cont_factor=1.0
        st=1
        if len(prof_data.shape)==3:st=0

        if inst==1:#Generic spectrograph/spectropolarimeter
            if wheigth_line=='all':
                for n,i in enumerate(np.arange((inwave[k]-l_core_pos)*stepwave[k],(fiwave[k]-l_core_pos)*stepwave[k]+0.000001,stepwave[k])):
                    if st==1:
                        fil.write(' '+line_indx_fin+'{:>15.3f}'.format(i)+'{:>15.5e}'.format(prof_data[n+int(inwave[k]),0,x,y])+'{:>15.5e}'.format(prof_data[n+int(inwave[k]),1,x,y])+'{:>15.5e}'.format(prof_data[n+int(inwave[k]),2,x,y])+'{:>15.5e}'.format(prof_data[n+int(inwave[k]),3,x,y])+'\n')
                    if st==0:
                        fil.write(' '+line_indx_fin+'{:>15.3f}'.format(i)+'{:>15.5e}'.format(prof_data[n+int(inwave[k]),x,y])+'{:>15.5e}'.format(0)+'{:>15.5e}'.format(0)+'{:>15.5e}'.format(0)+'\n')
            else:
                for n,i in enumerate(np.arange((inwave[k]-l_core_pos)*stepwave[k],(fiwave[k]-l_core_pos)*stepwave[k]+0.000001,stepwave[k])):
                    wh_i=1
                    for jk in range(len(wheigth_line)):
                        temp_range=wheigth_line[jk]
                        if temp_range[1]<=n+1<=temp_range[2]:
                            wh_i=temp_range[0]
                    if st==1:
                        fil.write(' '+line_indx_fin+'{:>15.3f}'.format(i)+'{:>15.5e}'.format(prof_data[n+int(inwave[k]),0,x,y])+'{:>15.5e}'.format(prof_data[n+int(inwave[k]),1,x,y])+'{:>15.5e}'.format(prof_data[n+int(inwave[k]),2,x,y])+'{:>15.5e}'.format(prof_data[n+int(inwave[k]),3,x,y])+'\n')
                    if st==0:
                        fil.write(' '+line_indx_fin+'{:>15.3f}'.format(i)+'{:>15.5e}'.format(prof_data[n+int(inwave[k]),x,y])+'{:>15.5e}'.format(0)+'{:>15.5e}'.format(0)+'{:>15.5e}'.format(0)+'\n')
    fil.close()

def convert_profile_two(ll,data,x,y,wave_leng):
    '''
    Create the .per files

    Parameters
    ----------
    first: array that you want to save
    second : Name of the fits file. You can add the path before name
    '''

    fil=open('profiles.per','w')
    for k in range(len(ll)):
        wheigth_line=wave_leng[k]
        line_indx_fin=ll[k].split(',')[0]
        inst=instindex
        prof_data=data[k]
        st=1
        if len(prof_data.shape)==3:st=0
        for n,i in enumerate(wheigth_line[0]):
            if st==1:
                fil.write(' '+line_indx_fin+'{:>15.3f}'.format(i)+'{:>15.5e}'.format(prof_data[n,0,x,y])+'{:>15.5e}'.format(prof_data[n,1,x,y])+'{:>15.5e}'.format(prof_data[n,2,x,y])+'{:>15.5e}'.format(prof_data[n,3,x,y])+'\n')
            if st==0:
                fil.write(' '+line_indx_fin+'{:>15.3f}'.format(i)+'{:>15.5e}'.format(prof_data[n,x,y])+'{:>15.5e}'.format(0)+'{:>15.5e}'.format(0)+'{:>15.5e}'.format(0)+'\n')
    fil.close()



def plot_maps(dat,dat2,dat3,fullp_chi2,output_opt):

    '''
    Plot and save the temperature and velocity maps on a pdf file

    Parameters
    ----------
    first: array that you want to save
    second : Name of the fits file. You can add the path before name
    '''




    pdf_pages = PdfPages('results_desire.pdf')



    fig = plt.figure(figsize=(18, 6))
    plt.figure(1)
    plt.subplot(131)
    intensity=dat3[2,0,:,:]


    if output_opt==1 or output_opt==2:
        intensity_fitted=dat2[2,0,:,:]
    if output_opt==3:
        intensity_fitted=dat2[-1,2,0,:,:]
    Z=intensity
    X,Y=np.meshgrid(range(Z.shape[1]),range(Z.shape[0]))
    plt.pcolormesh(X,Y,Z,cmap='gray',vmin=intensity.min(),vmax=intensity.max(),rasterized=True)


    cb = plt.colorbar()
    cb.set_label(label='Cont',fontsize=10)


    plt.subplot(132)


    Z=intensity_fitted
    X,Y=np.meshgrid(range(Z.shape[1]),range(Z.shape[0]))
    plt.pcolormesh(X,Y,Z,cmap='gray',vmin=intensity.min(),vmax=intensity.max(),rasterized=True)

    cb = plt.colorbar()
    cb.set_label(label='Fitted_cont_int',fontsize=10)

    plt.subplot(133)


    if output_opt==1 or output_opt==2:
        if nlte_f>=10:
            Z=fullp_chi2[:,:]
        if nlte_f<10:
            Z=fullp_chi2[0,:,:]
    if output_opt==3:
        if nlte_f>=10:
            Z=fullp_chi2[-1,:,:]
        if nlte_f<10:
            Z=fullp_chi2[-1,0,:,:]

    X,Y=np.meshgrid(range(Z.shape[1]),range(Z.shape[0]))
    plt.pcolormesh(X,Y,Z,vmin=0,cmap='gray',rasterized=True)

    cb = plt.colorbar()
    cb.set_label(label='I_chi2',fontsize=10)


    plt.tight_layout()

    pdf_pages.savefig(fig)
    plt.close()
    if output_opt==1 or output_opt==2:
        rang=len(dat[4,:,0,0])
    if output_opt==3:
        rang=len(dat[-1,4,:,0,0])
    for lt in range(0,rang,5):
        fig = plt.figure(figsize=(18, 6))
        op_dep_pos=lt
        if output_opt==1 or output_opt==2:
            magneticfield=dat[4,op_dep_pos,:,:]
        if output_opt==3:
            magneticfield=dat[-1,4,op_dep_pos,:,:]

        magneticfield[magneticfield==0] = np.nan
        magrma=np.nanmean(magneticfield)+3*np.nanstd(magneticfield)
        if magrma > np.max(magneticfield):
            magrma=np.max(magneticfield)




        if output_opt==1 or output_opt==2:
            temperature=dat[1,op_dep_pos,:,:]
        if output_opt==3:
            temperature=dat[-1,1,op_dep_pos,:,:]

        temperature[temperature==0] = np.nan
        temprma=np.nanmean(temperature)+3*np.nanstd(temperature)
        temprmi=np.nanmean(temperature)-3*np.nanstd(temperature)
        if temprma > np.max(temperature):
            temprma=np.max(temperature)

        if temprmi < np.min(temperature):
            temprmi=np.min(temperature)


        if output_opt==1 or output_opt==2:
            velocity=dat[5,op_dep_pos,:,:]/100000.
        if output_opt==3:
            velocity=dat[-1,5,op_dep_pos,:,:]/100000.
        velocity[velocity==0] = np.nan
        stdv=np.nanstd(velocity)


        plt.subplot(131)
        Z=temperature
        '''
        f = interp2d(range(Z.shape[0]), range(Z.shape[1]), Z.transpose(), kind='linear')
        x2 = np.linspace(0, len(range(Z.shape[0]))-1, (len(range(Z.shape[0]))-1)*4)
        y2 = np.linspace(0, len(range(Z.shape[1]))-1, (len(range(Z.shape[1]))-1)*4)
        Z2 = f(x2, y2)
        X2,Y2=np.meshgrid(x2,y2)
        '''
        plt.pcolormesh(X,Y,Z, cmap='hot',rasterized=True,vmin=temprmi,vmax=temprma)
        cb = plt.colorbar()

        cb.set_label(label='K',fontsize=10)
        if output_opt==1 or output_opt==2:
            plt.title('log tau '+str(dat[0,lt,0,0]))
        if output_opt==3:
            plt.title('log tau '+str(dat[-1,0,lt,0,0]))


        plt.subplot(132)
        Z=velocity
        '''
        f = interp2d(range(Z.shape[0]), range(Z.shape[1]), Z.transpose(), kind='linear')
        x2 = np.linspace(0, len(range(Z.shape[0]-1)), len(range(Z.shape[0]-1))*4)
        y2 = np.linspace(0, len(range(Z.shape[1]-1)), len(range(Z.shape[1]-1))*4)
        Z2 = f(x2, y2)
        X2,Y2=np.meshgrid(x2,y2)
        '''
        plt.pcolormesh(X,Y,Z,vmin=4*stdv*-1,vmax=4*stdv,cmap='bwr',rasterized=True)
        cb = plt.colorbar()

        cb.set_label(label='Km/s',fontsize=10)



        plt.subplot(133)
        Z=magneticfield
        '''
        f = interp2d(range(Z.shape[0]), range(Z.shape[1]), Z.transpose(), kind='linear')
        x2 = np.linspace(0, len(range(Z.shape[0]-1)), len(range(Z.shape[0]-1))*4)
        y2 = np.linspace(0, len(range(Z.shape[1]-1)), len(range(Z.shape[1]-1))*4)
        Z2 = f(x2, y2)
        X2,Y2=np.meshgrid(x2,y2)
        '''
        plt.pcolormesh(X,Y,Z, cmap='bone',vmin=0,vmax=magrma,rasterized=True)
        cb = plt.colorbar()
        cb.set_label(label='G',fontsize=10)

        plt.tight_layout()
        pdf_pages.savefig(fig)
        plt.close()

    firstPage = plt.figure(figsize=(11,10))
    firstPage.clf()
    ffi=open(run_file_folder+'desire.dtrol','r')
    txt = ffi.read()
    ffi.close()
    firstPage.text(0.05,0.95,txt, transform=firstPage.transFigure, size=12, va="top", ha="left")
    pdf_pages.savefig()
    plt.close()

    firstPage = plt.figure(figsize=(11,48))
    firstPage.clf()
    ffi=open(run_file_folder+'keyword.input','r')
    txt = ffi.read()
    ffi.close()
    firstPage.text(0.05,0.97,txt, transform=firstPage.transFigure, size=12, va="top", ha="left")
    pdf_pages.savefig()
    plt.close()

    pdf_pages.close()


def create_coor_file(arr,x,y):
    '''
    Create the .coor file

    Parameters
    ----------
    first: array that you want to save
    second : Name of the fits file. You can add the path before name
    '''
    filc=open('pos.coor','w')
    if type(arr[0]) == float:
        filc.write(str(arr[0])+','+str(arr[1])+','+str(arr[2])+','+str(arr[3]))
    if type(arr) == np.ndarray:
        filc.write(str(arr[0,x,y])+','+str(arr[1,x,y])+','+str(arr[2,x,y])+','+str(arr[3,x,y]))
    filc.close()


def vac_air_wave(wav):
    wav2=wav*wav*1.0
    nwave=wav
    if wav >=200:
        fact = 1.0 + 2.735182e-4 + (1.314182e0 + 2.76249e+4/wav2) / wav2
        fact2 = wav/fact
        return(fact2)
    else:
        return(nwave)


def create_folder(q):
    for i in range(q):
        os.chdir(sir_location)
        call('mkdir '+intern_folder_name+str(i+1), shell=True)
        os.chdir(intern_folder_name+str(i+1))
        call('cp '+run_file_folder+'*.* '+sir_location+intern_folder_name+str(i+1)+'/', shell=True)
        call('cp '+run_file_folder+'aux/* '+sir_location+intern_folder_name+str(i+1)+'/', shell=True)
        if clustering =='no':
            call('cp '+run_file_folder+'atmos/'+init_atmos_file_name+' '+sir_location+intern_folder_name+str(i+1)+'/', shell=True)
















def invert(t):
    folder_name=sir_location+intern_folder_name+str(multiprocessing.current_process()._identity[0])
    start2= time.time()
    k=t[0]
    i=t[1]
    pop_val_c=0
    os.chdir(folder_name)
    if instindex == 1:
        convert_profile_one(nindexa,instindex,core_pos,inwave,fiwave,stepwave,dataa,k,i,wave_leng)
    if instindex == 2:
        convert_profile_two(nindexa,dataa,k,i,wave_leng)

    create_coor_file(disk_pos,k,i)
    mod_file=open('profiles.per','r')
    kl=0

    for tline in mod_file:
        per_ori[:,kl,k-run_range_x_min,i-run_range_y_min]=tline.split()
        kl=kl+1
    mod_file.close()
    if clustering.isnumeric():
        chi2=10000000.
        chi2p=10000000.
        uatmos=-1
        for ati in range(int(clustering)):
            call('cp '+run_file_folder+'atmos/atmcl'+str(ati+1)+'.mod '+folder_name+'/'+init_atmos_file_name, shell=True)
            run_sir=subprocess.Popen('../../bin/desire desire.dtrol', shell = True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            out, err = run_sir.communicate()
            if os.path.exists(init_atmos_file_name_mo_mod+'_'+n_cycles+'.per'):
                fitted_prof=np.zeros((6,num_lines_per))
                mod_file2=open(init_atmos_file_name_mo_mod+'_'+n_cycles+'.per','r')
                kl=0
                for ik in mod_file2:
                    line=ik
                    fitted_prof[:,kl]=line.split()
                    kl=kl+1
                mod_file2.close()
                try:
                    qq=copy.deepcopy(per_ori[2,:,k-run_range_x_min,i-run_range_y_min])
                    qqq=fitted_prof[2,:]
                    qq[qq<0]=np.nan
                    qqq=qqq[~np.isnan(qq)]
                    qq=qq[~np.isnan(qq)]
                    qqq=qqq[qq>=0.01]
                    qq=qq[qq>=0.01]
                    chi2all=chisquare(qq,qqq)
                    chi2int=float(open('desire.chi','r').readlines()[-1].split()[1])
                except:
                    chi2int=1000000000.
                    chi2intp=1000000000.
            else:
                chi2int=1000000000.
                chi2intp=1000000000.
            if chi2int<chi2:
                atmcl[k-run_range_x_min,i-run_range_y_min]=ati+1
                chi2=chi2int
                if output_opt==1 or output_opt==2:
                    mod_file=open(init_atmos_file_name_mo_mod+'_'+n_cycles+'.mod','r')
                    mod_file.readline()
                    kl=0
                    for tline in mod_file:
                        inv_res_array_mod[:,kl,k-run_range_x_min,i-run_range_y_min]=tline.split()
                        kl=kl+1
                    mod_file.close()
                    mod_file2=open(init_atmos_file_name_mo_mod+'_'+n_cycles+'.per','r')
                    kl=0
                    for ik in mod_file2:
                        line=ik
                        inv_res_array_per[:,kl,k-run_range_x_min,i-run_range_y_min]=line.split()
                        kl=kl+1
                    mod_file2.close()
                    if nlte_f>=10:
                        try:
                            '''
                            qq=copy.deepcopy(per_ori[2,:,k-run_range_x_min,i-run_range_y_min])
                            qqq=inv_res_array_per[2,:,k-run_range_x_min,i-run_range_y_min]
                            qq[qq<0]=np.nan
                            qqq=qqq[~np.isnan(qq)]
                            qq=qq[~np.isnan(qq)]
                            qqq=qqq[qq>=0.01]
                            qq=qq[qq>=0.01]
                            chi2all=chisquare(qq,qqq)'''
                            chi_val=float(open('desire.chi','r').readlines()[-1].split()[1])
                            fullp_chi2[k-run_range_x_min,i-run_range_y_min]=chi_val
                        except:
                            fullp_chi2[k-run_range_x_min,i-run_range_y_min]=np.nan
                            inv_res_array_per[:,kl,k-run_range_x_min,i-run_range_y_min]=np.nan
                            inv_res_array_mod[:,kl,k-run_range_x_min,i-run_range_y_min]=np.nan


                    if nlte_f<10:
                        try:
                            '''
                            qq=copy.deepcopy(per_ori[2,:,k-run_range_x_min,i-run_range_y_min])
                            qqq=inv_res_array_per[2,:,k-run_range_x_min,i-run_range_y_min]
                            qq[qq<0]=np.nan
                            qqq=qqq[~np.isnan(qq)]
                            qq=qq[~np.isnan(qq)]
                            qqq=qqq[qq>=0.01]
                            qq=qq[qq>=0.01]
                            chi2all=chisquare(qq,qqq)'''
                            chi_val=float(open('desire.chi','r').readlines()[-1].split()[1])
                            fullp_chi2[0,k-run_range_x_min,i-run_range_y_min]=chi_val
                        except:
                            fullp_chi2[0,k-run_range_x_min,i-run_range_y_min]=np.nan
                            inv_res_array_per[:,kl,k-run_range_x_min,i-run_range_y_min]=np.nan
                            inv_res_array_mod[:,kl,k-run_range_x_min,i-run_range_y_min]=np.nan
                        try:
                            pop_val_c=float([s for s in err.decode("utf-8").split('\n') if "delta" in s][-1].split('=')[1].replace('(accelerated)',''))
                            fullp_chi2[1,k-run_range_x_min,i-run_range_y_min]=pop_val_c
                        except:
                            None

                if output_opt==2:
                    kl=0
                    mod_file_err=open(init_atmos_file_name_mo_mod+'_'+n_cycles+'.err','r')
                    mod_file_err.readline()
                    kl=0
                    for tline in mod_file_err:
                        inv_error_mod[:,kl,k-run_range_x_min,i-run_range_y_min]=tline.split()
                        kl=kl+1
                    mod_file.close()

                if output_opt==3:
                    for ii in range(n_cycles):
                        mod_file=open(init_atmos_file_name_mo_mod+'_'+ii+'.mod','r')
                        mod_file.readline()
                        kl=0
                        for tline in mod_file:
                            inv_res_array_mod[ii,:,kl,k-run_range_x_min,i-run_range_y_min]=tline.split()
                            kl=kl+1
                        mod_file.close()
                        mod_file2=open(init_atmos_file_name_mo_mod+'_'+ii+'.per','r')
                        kl=0
                        for ik in mod_file2:
                            line=ik
                            inv_res_array_per[ii,:,kl,k-run_range_x_min,i-run_range_y_min]=line.split()
                            kl=kl+1
                        mod_file2.close()

                        if nlte_f>=10:
                            try:
                                '''
                                qq=copy.deepcopy(per_ori[2,:,k-run_range_x_min,i-run_range_y_min])
                                qqq=inv_res_array_per[ii,2,:,k-run_range_x_min,i-run_range_y_min]
                                qq[qq<0]=np.nan
                                qqq=qqq[~np.isnan(qq)]
                                qq=qq[~np.isnan(qq)]
                                qqq=qqq[qq>=0.01]
                                qq=qq[qq>=0.01]
                                chi2all=chisquare(qq,qqq)
                                '''
                                chi_val=float(open('desire.chi','r').readlines()[-1].split()[1])
                                fullp_chi2[ii,k-run_range_x_min,i-run_range_y_min]=chi_val
                            except:
                                fullp_chi2[ii,k-run_range_x_min,i-run_range_y_min]=np.nan
                                inv_res_array_per[ii,:,kl,k-run_range_x_min,i-run_range_y_min]=np.nan
                                inv_res_array_mod[ii,:,kl,k-run_range_x_min,i-run_range_y_min]=np.nan


                        if nlte_f<10:
                            try:
                                qq=copy.deepcopy(per_ori[2,:,k-run_range_x_min,i-run_range_y_min])
                                qqq=inv_res_array_per[ii,2,:,k-run_range_x_min,i-run_range_y_min]
                                qq[qq<0]=np.nan
                                qqq=qqq[~np.isnan(qq)]
                                qq=qq[~np.isnan(qq)]
                                qqq=qqq[qq>=0.01]
                                qq=qq[qq>=0.01]
                                chi2all=chisquare(qq,qqq)
                                chi_val=float(open('desire.chi','r').readlines()[-1].split()[1])
                                fullp_chi2[ii,0,k-run_range_x_min,i-run_range_y_min]=chi_val
                            except:
                                fullp_chi2[ii,0,k-run_range_x_min,i-run_range_y_min]=np.nan
                                inv_res_array_per[ii,:,kl,k-run_range_x_min,i-run_range_y_min]=np.nan
                                inv_res_array_mod[ii,:,kl,k-run_range_x_min,i-run_range_y_min]=np.nan
                            try:
                                pop_val_c=float([s for s in err.decode("utf-8").split('\n') if "delta" in s][-1].split('=')[1].replace('(accelerated)',''))
                                fullp_chi2[ii,1,k-run_range_x_min,i-run_range_y_min]=pop_val_c
                            except:
                                pop_val_c=0


            if os.path.exists(init_atmos_file_name_mo_mod+'_'+n_cycles+'.per'):
                if chi2all[0]<chi2p:uatmos=ati
                if chi2all[0]<chi2p:chi2p=chi2all[0]
                if nlte_f<10:mensagem='chi2='+'{:>2.5f}'.format(chi2p)+'  rh_delta='+'{:>2.1e}'.format(pop_val_c)+'  with atmos  '+'{:>2.0f}'.format(uatmos)
                if nlte_f>=10:mensagem='chi2='+'{:>2.5f}'.format(chi2p)+'  with atmos  '+'{:>2.0f}'.format(uatmos)
            else:
                mensagem='No convergence'
                file = open(dir_path+'/results/errors/pixel_'+str(i)+'_'+str(k)+'.txt', 'w')
                file.write(out.decode("utf-8"))
                file.write('\n')
                file.write('----------------------------------------------------------')
                file.write('\n')
                file.write(err.decode("utf-8"))
                file.close()
            call('rm inver.chi', shell=True)
            print('x-'+'{:>3.0f}'.format(k),'   y-'+'{:>3.0f}'.format(i),'atmos_model'+'{:>3.0f}'.format(ati),'{:>10.1f}'.format(time.time()-start2),'s','{:>10.2f}'.format(((i-run_range_y_min)+1+((k-run_range_x_min)*(run_range_y_max-run_range_y_min*1.0)))/((run_range_x_max-run_range_x_min*1.0)*(run_range_y_max-run_range_y_min))*100),'%    ', mensagem)
            if coorde[0] == '0':
                call('cp -r * '+dir_path+'/results/run_example/', shell=True)
            shp = subprocess.Popen('rm '+init_atmos_file_name_mo_mod+'_'+n_cycles+'.per', shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            outs, errs = shp.communicate()


































    else:
        if clustering =='yes':
            clust_ind=int(clust_ind_arr[k,i])
            call('cp '+run_file_folder+'atmos/atmcl'+str(clust_ind)+'.mod '+folder_name+'/'+init_atmos_file_name, shell=True)
        if clustering =='atmos':
            np.savetxt(init_atmos_file_name,ml_atmos[:,:,k,i].transpose(),fmt='%1.5e', delimiter=' ', newline='\n', header=atmos_header[0],comments='')
        if clustering =='nn':
            np.savetxt(init_atmos_file_name,ml_atmos[:,:,k,i].transpose(),fmt='%1.5e', delimiter=' ', newline='\n', header=atmos_header[0],comments='')
        run_sir=subprocess.Popen('../../bin/desire desire.dtrol', shell = True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = run_sir.communicate()
        if os.path.exists(init_atmos_file_name_mo_mod+'_'+n_cycles+'.per'):
            if output_opt==1 or output_opt==2:
                mod_file=open(init_atmos_file_name_mo_mod+'_'+n_cycles+'.mod','r')
                mod_file.readline()
                kl=0
                for tline in mod_file:
                    inv_res_array_mod[:,kl,k-run_range_x_min,i-run_range_y_min]=tline.split()
                    kl=kl+1
                mod_file.close()
                mod_file2=open(init_atmos_file_name_mo_mod+'_'+n_cycles+'.per','r')
                kl=0
                for ik in mod_file2:
                    line=ik
                    inv_res_array_per[:,kl,k-run_range_x_min,i-run_range_y_min]=line.split()
                    kl=kl+1
                mod_file2.close()
                if nlte_f>=10:
                    try:
                        qq=copy.deepcopy(per_ori[2,:,k-run_range_x_min,i-run_range_y_min])
                        qqq=inv_res_array_per[2,:,k-run_range_x_min,i-run_range_y_min]
                        qq[qq<0]=np.nan
                        qqq=qqq[~np.isnan(qq)]
                        qq=qq[~np.isnan(qq)]
                        qqq=qqq[qq>=0.01]
                        qq=qq[qq>=0.01]
                        chi2all=chisquare(qq,qqq)[0]
                        #chi_val=float(open('desire.chi','r').readlines()[-1].split()[1])
                        fullp_chi2[k-run_range_x_min,i-run_range_y_min]=chi2all
                    except:
                        fullp_chi2[k-run_range_x_min,i-run_range_y_min]=np.nan
                        inv_res_array_per[:,kl,k-run_range_x_min,i-run_range_y_min]=np.nan
                        inv_res_array_mod[:,kl,k-run_range_x_min,i-run_range_y_min]=np.nan


                if nlte_f<10:
                    try:
                        qq=copy.deepcopy(per_ori[2,:,k-run_range_x_min,i-run_range_y_min])
                        qqq=inv_res_array_per[2,:,k-run_range_x_min,i-run_range_y_min]
                        qq[qq<0]=np.nan
                        qqq=qqq[~np.isnan(qq)]
                        qq=qq[~np.isnan(qq)]
                        qqq=qqq[qq>=0.01]
                        qq=qq[qq>=0.01]
                        chi2all=chisquare(qq,qqq)[0]
                        #chi_val=float(open('desire.chi','r').readlines()[-1].split()[1])
                        fullp_chi2[0,k-run_range_x_min,i-run_range_y_min]=chi2all
                    except:
                        fullp_chi2[0,k-run_range_x_min,i-run_range_y_min]=np.nan
                        inv_res_array_per[:,kl,k-run_range_x_min,i-run_range_y_min]=np.nan
                        inv_res_array_mod[:,kl,k-run_range_x_min,i-run_range_y_min]=np.nan
                    try:
                        pop_val_c=float([s for s in err.decode("utf-8").split('\n') if "delta" in s][-1].split('=')[1].replace('(accelerated)',''))
                        fullp_chi2[1,k-run_range_x_min,i-run_range_y_min]=pop_val_c
                    except:
                        None

            if output_opt==2:
                kl=0
                mod_file_err=open(init_atmos_file_name_mo_mod+'_'+n_cycles+'.err','r')
                mod_file_err.readline()
                kl=0
                for tline in mod_file_err:
                    inv_error_mod[:,kl,k-run_range_x_min,i-run_range_y_min]=tline.split()
                    kl=kl+1
                mod_file.close()

            if output_opt==3:
                for ii in range(n_cycles):
                    mod_file=open(init_atmos_file_name_mo_mod+'_'+ii+'.mod','r')
                    mod_file.readline()
                    kl=0
                    for tline in mod_file:
                        inv_res_array_mod[ii,:,kl,k-run_range_x_min,i-run_range_y_min]=tline.split()
                        kl=kl+1
                    mod_file.close()
                    mod_file2=open(init_atmos_file_name_mo_mod+'_'+ii+'.per','r')
                    kl=0
                    for ik in mod_file2:
                        line=ik
                        inv_res_array_per[ii,:,kl,k-run_range_x_min,i-run_range_y_min]=line.split()
                        kl=kl+1
                    mod_file2.close()

                    if nlte_f>=10:
                        try:
                            '''
                            qq=qq=copy.deepcopy(per_ori[2,:,k-run_range_x_min,i-run_range_y_min])
                            qqq=inv_res_array_per[ii,2,:,k-run_range_x_min,i-run_range_y_min]
                            qq[qq<0]=np.nan
                            qqq=qqq[~np.isnan(qq)]
                            qq=qq[~np.isnan(qq)]
                            qqq=qqq[qq>=0.01]
                            qq=qq[qq>=0.01]
                            chi2all=chisquare(qq,qqq)'''
                            chi_val=float(open('inver.chi','r').readlines()[-1].split()[1])
                            fullp_chi2[ii,k-run_range_x_min,i-run_range_y_min]=chi_val
                        except:
                            fullp_chi2[ii,k-run_range_x_min,i-run_range_y_min]=np.nan
                            inv_res_array_per[ii,:,kl,k-run_range_x_min,i-run_range_y_min]=np.nan
                            inv_res_array_mod[ii,:,kl,k-run_range_x_min,i-run_range_y_min]=np.nan


                    if nlte_f<10:
                        try:
                            '''
                            qq=qq=copy.deepcopy(per_ori[2,:,k-run_range_x_min,i-run_range_y_min])
                            qqq=inv_res_array_per[ii,2,:,k-run_range_x_min,i-run_range_y_min]
                            qq[qq<0]=np.nan
                            qqq=qqq[~np.isnan(qq)]
                            qq=qq[~np.isnan(qq)]
                            qqq=qqq[qq>=0.01]
                            qq=qq[qq>=0.01]
                            chi2all=chisquare(qq,qqq)'''
                            chi_val=float(open('inver.chi','r').readlines()[-1].split()[1])
                            fullp_chi2[ii,0,k-run_range_x_min,i-run_range_y_min]=chi_val
                        except:
                            fullp_chi2[ii,0,k-run_range_x_min,i-run_range_y_min]=np.nan
                            inv_res_array_per[ii,:,kl,k-run_range_x_min,i-run_range_y_min]=np.nan
                            inv_res_array_mod[ii,:,kl,k-run_range_x_min,i-run_range_y_min]=np.nan
                        try:
                            pop_val_c=float([s for s in err.decode("utf-8").split('\n') if "delta" in s][-1].split('=')[1].replace('(accelerated)',''))
                            fullp_chi2[ii,1,k-run_range_x_min,i-run_range_y_min]=pop_val_c
                        except:
                            pop_val_c=0

        if os.path.exists(init_atmos_file_name_mo_mod+'_'+n_cycles+'.per'):
            if nlte_f<10:mensagem='chi2='+'{:>2.3f}'.format(chi2all)+'  rh_delta='+'{:>2.1e}'.format(pop_val_c)
            if nlte_f>=10:mensagem='chi2='+'{:>2.3f}'.format(chi2all)
        else:
            mensagem='No convergence'
            file = open(dir_path+'/results/errors/pixel_'+str(i)+'_'+str(k)+'.txt', 'w')
            file.write(out.decode("utf-8"))
            file.write('\n')
            file.write('----------------------------------------------------------')
            file.write('\n')
            file.write(err.decode("utf-8"))
            file.close()
        print('x-'+'{:>3.0f}'.format(k),'   y-'+'{:>3.0f}'.format(i),'{:>10.1f}'.format(time.time()-start2),'s','{:>10.2f}'.format(((i-run_range_y_min)+1+((k-run_range_x_min)*(run_range_y_max-run_range_y_min*1.0)))/((run_range_x_max-run_range_x_min*1.0)*(run_range_y_max-run_range_y_min))*100),'%    ', mensagem)
        if coorde[0] == '0':
            call('cp -r * '+dir_path+'/results/run_example/', shell=True)
        shp = subprocess.Popen('rm '+init_atmos_file_name_mo_mod+'_'+n_cycles+'.per', shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        outs, errs = shp.communicate()
    return()





def sint(t):
    os.chdir(sir_location)
    start2= time.time()
    k=t[1]
    i=t[0]
    call('mkdir d_'+str(k)+'_'+str(i), shell=True)
    os.chdir('d_'+str(k)+'_'+str(i))
    call('cp '+run_file_folder+'*.* '+sir_location+'d_'+str(k)+'_'+str(i)+'/', shell=True)
    call('cp '+run_file_folder+'aux/* '+sir_location+'d_'+str(k)+'_'+str(i)+'/', shell=True)
    call('rm '+init_atmos_file_name, shell=True)
    modatmos=dataa[0]
    np.savetxt(init_atmos_file_name,modatmos[:,:,k,i].transpose(),fmt='%1.5e', delimiter=' ', newline='\n', header=atmos_header[0],comments='')
    run_sir=subprocess.Popen('../../bin/desire desire.dtrol', shell = True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = run_sir.communicate()
    if os.path.exists('profiles.per'):
        mod_file2=open('profiles.per','r')
        kl=0
        for ik in mod_file2:
            line=ik
            inv_res_array_per[:,kl,k-run_range_x_min,i-run_range_y_min]=line.split()
            kl=kl+1
        mod_file2.close()
    else:
        None
    if os.path.exists('profiles.per'):
        mensagem='Converged'
    else:
        mensagem='No convergence'
        file = open(dir_path+'/results/errors/pixel_'+str(i)+'_'+str(k)+'.txt', 'w')
        file.write(out.decode("utf-8"))
        file.write('\n')
        file.write('----------------------------------------------------------')
        file.write('\n')
        file.write(err.decode("utf-8"))
        file.close()
#    print('x-'+'{:>3.0f}'.format(k),'   y-'+'{:>3.0f}'.format(i),'{:>10.1f}'.format(time.time()-start2),'s','{:>10.2f}'.format(((i-run_range_y_min)+1+((k-run_range_x_min)*(run_range_y_max-run_range_y_min*1.0)))/((run_range_x_max-run_range_x_min*1.0)*(run_range_y_max-run_range_y_min))*100),'%    ', mensagem)
    print('x-'+'{:>3.0f}'.format(k),'   y-'+'{:>3.0f}'.format(i),'{:>10.1f}'.format(time.time()-start2),'s','{:>10.2f}'.format(((run_range_x_max-run_range_x_min*1.0)*(run_range_y_max-run_range_y_min)*100)),'%    ', mensagem)
    call('rm -r ../d_'+str(k)+'_'+str(i), shell=True)
    return()


def rffp(t):
    os.chdir(sir_location)
    start2= time.time()
    k=t[1]
    i=t[0]
    call('mkdir d_'+str(k)+'_'+str(i), shell=True)
    os.chdir('d_'+str(k)+'_'+str(i))
    call('cp '+run_file_folder+'*.* '+sir_location+'d_'+str(k)+'_'+str(i)+'/', shell=True)
    call('cp '+run_file_folder+'aux/* '+sir_location+'d_'+str(k)+'_'+str(i)+'/', shell=True)
    call('rm '+init_atmos_file_name, shell=True)
    modatmos=dataa[0]
    np.savetxt(init_atmos_file_name,modatmos[:,:,k,i].transpose(),fmt='%1.5e', delimiter=' ', newline='\n', header=atmos_header[0],comments='')
    run_sir=subprocess.Popen('../../bin/desire desire.dtrol', shell = True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = run_sir.communicate()

    if os.path.exists(glob.glob('initat.r*')[0]):
        mod_file2=open(glob.glob('initat.r*')[0],'r')
        kl=0
        nada=mod_file2.readline()
        param_rf=nada.split()
        rf_temp=np.loadtxt(glob.glob('initat.r*')[0],skiprows=1)
        rf2=rf_temp.reshape(param_rf[0],int(param_rf[1]/4),4,order='C')
        rf[0,:,:,k-run_range_x_min,i-run_range_y_min]=rf2[:,:,0]
        rf[1,:,:,k-run_range_x_min,i-run_range_y_min]=rf2[:,:,2]
        rf[2,:,:,k-run_range_x_min,i-run_range_y_min]=rf2[:,:,3]
        rf[3,:,:,k-run_range_x_min,i-run_range_y_min]=rf2[:,:,4]

        mod_file2.close()
    else:
        None

    if os.path.exists(glob.glob('initat.r*')[0]):
        mensagem='Converged'
    else:
        mensagem='No convergence'
        file = open(dir_path+'/results/errors/pixel_'+str(i)+'_'+str(k)+'.txt', 'w')
        file.write(out.decode("utf-8"))
        file.write('\n')
        file.write('----------------------------------------------------------')
        file.write('\n')
        file.write(err.decode("utf-8"))
        file.close()
    print('x-'+'{:>3.0f}'.format(k),'   y-'+'{:>3.0f}'.format(i),'{:>10.1f}'.format(time.time()-start2),'s','{:>10.2f}'.format(((i-run_range_y_min)+1+((k-run_range_x_min)*(run_range_y_max-run_range_y_min*1.0)))/((run_range_x_max-run_range_x_min*1.0)*(run_range_y_max-run_range_y_min))*100),'%    ', mensagem)
    call('rm -r ../d_'+str(k)+'_'+str(i), shell=True)
    return()




def nn_init(to_fit_spectra2,xf,xi,yf,yi,ncores):

    np.random.seed(123)
    from keras.models import Sequential
    from keras.layers import Dense, Dropout, Activation, Flatten
    from keras.layers import Conv1D, MaxPooling1D
    from keras.utils import np_utils
    from keras.models import load_model
    from keras.callbacks import ModelCheckpoint
    from keras.activations import relu
    from keras.optimizers import Adam,Adamax
    from keras.utils import plot_model
    os.chdir(run_file_folder+'/atmos/')
    shapespectpre=to_fit_spectra2.shape
    n_nodes=10
    try:
        filepath = glob.glob('*.h5')[0]
    except:
        print('Neural network model file not found.')
    low_atmos=float(filepath.replace('.','_').split('_')[2])*(0.10)
    top_atmos=float(filepath.replace('.','_').split('_')[3])*(-0.10)
    n_batch=64
    global nodes_val
    nodes_val=np.arange(low_atmos,top_atmos-0.001,-0.1)
    global nodes
    nodes=np.linspace(0,len(nodes_val)-1,n_nodes,dtype = int)
    v_norm=30*100000.
    t_norm=20000
    g_norm=6000

    shapespect=to_fit_spectra2.shape
    nl=shapespect[0]
    total_point_atmos=len(nodes)*3+2

    npx=xf-xi
    npy=xf-xi
    to_fit_spectra=np.zeros((npx*npy,shapespect[0],4))

    n=0

    for i in range(xi,xf):
      for l in range(yi,yf):
          to_fit_spectra[n,:,0]=to_fit_spectra2[:,0,i,l]
          to_fit_spectra[n,:,1]=to_fit_spectra2[:,1,i,l]
          to_fit_spectra[n,:,2]=to_fit_spectra2[:,2,i,l]
          to_fit_spectra[n,:,3]=to_fit_spectra2[:,3,i,l]*5
          n+=1




    def func(x, a, b, c):
      return a * np.exp(-b * x) + c



    list_run=[]
    n=0
    print(npx,npy)
    for i in range(npx):
      for l in range(npy):
          list_run.append((i,l,n))
          n=n+1
    global atmos_full
    atmos_full_base= multiprocessing.Array(ctypes.c_double, (11*len(nodes_val)*npx*npy))
    atmos_full= np.ctypeslib.as_array(atmos_full_base.get_obj())
    atmos_full= atmos_full.reshape(11,len(nodes_val),npx,npy)
    atmos_full[:,:,:,:]=1

    model = load_model(filepath)
    global to_fit_atmos
    to_fit_atmos = np.ones(([npy*npy,total_point_atmos]))
    to_fit_atmos = model.predict(to_fit_spectra)

    p = multiprocessing.Pool(ncores)
    p.map(get_map, list_run,chunksize=1)
    hdu = fits.PrimaryHDU(atmos_full)
    hdu.writeto('atmos.fits',overwrite=True)
    return()



def get_map(t):
    from scipy.interpolate import interp1d
    from scipy.optimize import curve_fit
    i=t[0]
    l=t[1]
    v_norm=30*100000.
    t_norm=20000
    g_norm=6000
    xnew = np.arange(0, len(nodes_val), 1)
    nones=np.ones(len(xnew))
    n=t[2]
    atmos_full[0,:,i,l]=nodes_val
    it=interp1d(nodes,to_fit_atmos[n,:len(nodes)]*t_norm, kind='cubic')
    atmos_full[1,:,i,l]=it(xnew)
    point_to_int=to_fit_atmos[n,len(nodes):len(nodes)+len(nodes)]*g_norm
    ig=interp1d(nodes,point_to_int, kind='linear')
    atmos_full[4,:,i,l]=abs(ig(xnew))

    point_to_int=(to_fit_atmos[n,len(nodes)+len(nodes):len(nodes)+len(nodes)+len(nodes)]-0.5)*v_norm
    iv=interp1d(nodes,point_to_int, kind='linear')
    atmos_full[5,:,i,l]=iv(xnew)

    #atmos_full[2,:,i,l]=train_set_atmos2[2,:,i,l]
    atmos_full[3,:,i,l]=1#train_set_atmos2[3,:,i,l]
    atmos_full[6,:,i,l]=nones*to_fit_atmos[n,len(nodes)+len(nodes)+len(nodes):len(nodes)+len(nodes)+len(nodes)+1]*180
    atmos_full[7,:,i,l]=nones*to_fit_atmos[n,len(nodes)+len(nodes)+len(nodes)+1:len(nodes)+len(nodes)+len(nodes)+1+1]*360
    return()



def fix_path(path):
    path = repr(path)
    path = path.replace(")", "\)")
    path = path.replace("(", "\(")
    path = path.replace(" ", "\ ")
    path = os.path.abspath(path).split("'")[1]
    return path

#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------





dir_path = os.path.dirname(os.path.realpath(__file__))
dir_path=fix_path(dir_path)


home=expanduser("~")
intern_folder_name='d_'+str(time.time()).split('.')[0][-4:]+'_'
f=open('initiallization.input','r')
Input_file=f.readlines()
f.close()


init_file_keys=['Ncores=','lindex=','instindex=','data=','core_position=','inwave=','fiwave=','stepwave=','wavewheig=','coord=','...????','...????','...????','Continuum_w???????=','Position_disk_obs=','clustering=','run_files=','DeSIRe_V=','output_type=','Email_adress=','Atmospheric_model_header=','clustering=']
init_file_pos=np.zeros(len(init_file_keys))

for i in range(len(init_file_keys)):
    for j in range(len(Input_file)):
        if Input_file[j].find(init_file_keys[i]) >=0:
            init_file_pos[i]=j



#Select the location of SIR version and run files
init_files=Input_file[int(init_file_pos[16])].replace("\n", "").split('=')[1].split(',')
sir_location=Input_file[int(init_file_pos[17])].replace("\n", "").split('=')[1].split(',')

if len(sir_location[0]) >1:
    sir_location=sir_location[0]
else:
    sir_location=home+'/desire/run/'
if len(init_files[0]) >1:
    run_file_folder=init_files[0]
else:
    run_file_folder=script_folder+'/run_files/'

#Identify the number of cores or syntesis mode and atmos file name
ff=open(run_file_folder+'desire.dtrol','r')
input_drtrol=ff.readlines()
ff.close()
n_cycles=(input_drtrol[0].replace("\n", "").replace(" ","").split(':')[1]).split('!')[0]
n_cycles2=int(n_cycles)
init_atmos_file_name=(input_drtrol[7].replace("\n", "").replace(" ","").split(':')[1]).split('!')[0]
init_atmos_file_name_mo_mod=init_atmos_file_name.split('.')[0]


nlte_f=(input_drtrol[40].replace("\n", "").replace(" ","").split(':')[1]).split('!')[0]
nlte_f=float(nlte_f.replace(" ", ""))


ncores=int(Input_file[int(init_file_pos[0])].replace("\n", "").split('=')[1])
ninde=Input_file[int(init_file_pos[1])].replace("\n", "").split('=')[1].split(',')
nlines=len(ninde)

#Select line index from LINES file
nindexa={}
for i in range(len(ninde)):
    nindexa[i]=ninde[i].replace(":", ",")
instindex=list(map(int,Input_file[int(init_file_pos[2])].replace("\n", "").split('=')[1].split(',')))[0]



#Read data
data_path=Input_file[int(init_file_pos[3])].replace("\n", "").split('=')[1].split(',')
dataa={}
shapea={}
for n,k in enumerate(nindexa):
    temp_dat=fits.open(data_path[n])[0]
    temp_dat=temp_dat.data
    shapea[k]=temp_dat.shape
    dataa[k]=temp_dat











if instindex == 1:
    shp = subprocess.Popen('rm '+run_file_folder+'*.grid', shell = True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    outs, errs = shp.communicate()
    shp = subprocess.Popen('rm '+run_file_folder+'*.wht', shell = True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    outs, errs = shp.communicate()
    #Select wavelength parameters
    inwave=np.zeros(nlines)
    fiwave=np.zeros(nlines)
    for i in range(nlines):
            fiwave[i]=shapea[i][0]-1
    #Select the line core position
    core_pos=list(map(float,Input_file[int(init_file_pos[4])].replace("\n", "").split('=')[1].split(',')))
    #select the wheigth and wavelength
    wave_leng={}
    wavelist=Input_file[int(init_file_pos[8])].replace("\n", "").split('=')[1].replace(']],[[',']] [[').split('] [')
    for i in range(nlines):
        wavelist_temp=wavelist[i].replace('[[','[').replace(']]',']').split('],[')
        range_list=np.zeros((len(wavelist_temp),3))
        for j in range(len(wavelist_temp)):
            wavelist_temp2=wavelist_temp[j].replace(']','').replace('[','').replace('[[','').replace(']]','').split(',')
            if wavelist_temp2[0] == 'all':
                wave_leng[i] ='all'
            else:
                range_list[j,:] =list(map(float,wavelist_temp2))
        if wavelist_temp2[0] != 'all': wave_leng[i]=range_list
    stepwave_temp=Input_file[int(init_file_pos[7])].replace("\n", "").split('=')[1].split(',')
    stepwave=np.zeros((len(stepwave_temp)))
    for n in nindexa:
            stepwave[n]=float(stepwave_temp[n])

if instindex == 2:
    #Select wavelength parameters
    shp = subprocess.Popen('cp '+run_file_folder+'*.grid '+run_file_folder+'aux' , shell = True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    outs, errs = shp.communicate()
    shp = subprocess.Popen('cp '+run_file_folder+'*.wht '+run_file_folder+'aux', shell = True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    outs, errs = shp.communicate()
    try:
        f=np.loadtxt(run_file_folder+'profiles.wht')
    except:
        print('No wht file detected.')
        sys.exit("Exiting computation")
    inwave=np.zeros(nlines)
    fiwave=np.zeros(nlines)


    #select the wheigth and wavelength
    wave_leng={}
    for i in range(nlines):
        wave_leng[i]=f[np.where(f[:,0]==(int(ninde[i].split(':')[0]))),1]




#Select x and y
#coorde=list(map(int,Input_file[int(init_file_pos[9])].replace("\n", "").split('=')[1].split(',')))[0]
coorde=Input_file[int(init_file_pos[9])].replace("\n", "").split('=')[1]


if coorde[0] == '0':
    run_range_x_min=0
    run_range_y_min=0
    run_range_x_max=1
    run_range_y_max=1
if coorde[0] == '1':
    run_range_x_min=0
    run_range_y_min=0
    run_range_x_max=shapea[0][-2]
    run_range_y_max=shapea[0][-1]
if coorde[0] == '[':
    run_range_x_min=int(coorde.replace('[','').replace(']','').split(',')[2])
    run_range_y_min=int(coorde.replace('[','').replace(']','').split(',')[0])
    run_range_x_max=int(coorde.replace('[','').replace(']','').split(',')[3])
    run_range_y_max=int(coorde.replace('[','').replace(']','').split(',')[1])






if n_cycles2>0 or n_cycles2 ==-1:
    #Set the clustering
    clustering=Input_file[int(init_file_pos[15])].replace("\n", "").split('=')[1].split(',')[0]
    clustering=clustering.lower()
    if clustering =='yes':
        clust_ind_arr=fits.open(run_file_folder+'/atmos/profile_clustering.fits')[0]
        clust_ind_arr=clust_ind_arr.data
    if clustering =='nn':
        nn_init(dataa[0],run_range_x_max,run_range_x_min,run_range_y_max,run_range_y_min,ncores)
        ml_atmos=fits.open(run_file_folder+'/atmos/atmos.fits')[0]
        ml_atmos=ml_atmos.data
        del to_fit_atmos
        del atmos_full
    if clustering =='atmos':
        ml_atmos=fits.open(run_file_folder+'/atmos/atmos.fits')[0]
        ml_atmos=ml_atmos.data

    #Select saving the spectra option
    output_opt=int(Input_file[int(init_file_pos[18])].replace("\n", "").split('=')[1])

#Select the solar disk pixel position. coor file
disk_pos=Input_file[int(init_file_pos[14])].replace("\n", "").split('=')[1].replace('[','').replace(']','').split(',')


try:
    ver_int=float(disk_pos[0])
    disk_pos=list(map(float,disk_pos))
except:
    temp_coor=fits.open(disk_pos[0])[0]
    disk_pos=temp_coor.data

#Select atmosphere header in case of syntesis
atmos_header=Input_file[int(init_file_pos[20])].replace("\n", "").split('=')[1].split(',')
















#Read notification email
email_adre=Input_file[int(init_file_pos[19])].replace("\n", "").split('=')[1].split(',')




start= time.time()

os.chdir(sir_location)

#Creat results and erros folder
if os.path.exists(dir_path+'/results/'):
    None
else:
    call('mkdir '+dir_path+'/results/', shell=True)

if os.path.exists(dir_path+'/results/errors/'):
    if glob.glob(dir_path+'/results/errors/*.txt'):
        call('rm '+dir_path+'/results/errors/*', shell=True)
else:
    call('mkdir '+dir_path+'/results/errors/', shell=True)






#Create the global arrays where the data is stored
if instindex == 1:
    make_grid_file_one(run_file_folder+'aux/',nindexa,stepwave,inwave,fiwave,core_pos,instindex,wave_leng)

if instindex == 2:
    make_grid_file_two(run_file_folder+'aux/',nindexa,wave_leng)



if (n_cycles2>0 or n_cycles2==-1) and instindex==1:
    fil=open(run_file_folder+'aux/'+'profiles.wht','w')
    for k in range(len(nindexa)):
        wheigth_line=wave_leng[k]
        line_indx_fin=nindexa[k].split(',')[0]
        inst=instindex
        l_core_pos=core_pos[k]
        if inst==1:#Generic spectrograph/spectropolarimeter
            if wheigth_line=='all':
                for n,i in enumerate(np.arange((inwave[k]-l_core_pos)*stepwave[k],(fiwave[k]-l_core_pos)*stepwave[k]+0.000001,stepwave[k])):
                    fil.write(' '+str(line_indx_fin)+'{:>15.5f}'.format(i)+'{:>15.5e}'.format(1)+'{:>15.5e}'.format(1)+'{:>15.5e}'.format(1)+'{:>15.5e}'.format(1)+'\n')
            else:
                for n,i in enumerate(np.arange((inwave[k]-l_core_pos)*stepwave[k],(fiwave[k]-l_core_pos)*stepwave[k]+0.000001,stepwave[k])):
                    wh_i=1
                    for jk in range(len(wheigth_line)):
                        temp_range=wheigth_line[jk]
                        if temp_range[1]<=n+1<=temp_range[2]:
                            wh_i=temp_range[0]
                    fil.write(' '+str(line_indx_fin)+'{:>15.5f}'.format(i)+'{:>15.5e}'.format(wh_i)+'{:>15.5e}'.format(wh_i)+'{:>15.5e}'.format(wh_i)+'{:>15.5e}'.format(wh_i)+'\n')
    fil.close()







if n_cycles2>0:
    atmos_files=glob.glob(run_file_folder+'/atmos/*.mod')
    num_lines_mod = sum(1 for line in open(atmos_files[0]))-1
    num_lines_per= sum(1 for line in open(run_file_folder+'aux/'+'profiles.wht'))

if n_cycles2==0:
    num_lines_per=int(1+(fiwave[0]-inwave[0])/stepwave[0])


#Clean folder from previous runs
#shp = subprocess.Popen('rm -r d_*', shell = True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
#outs, errs = shp.communicate()



if coorde[0] != '2':
    list_run=[]
    for i in range(run_range_x_min,run_range_x_max):
        for l in range(run_range_y_min,run_range_y_max):
            if type(disk_pos) == np.ndarray:
                if disk_pos[0,i,l]>=0:
                    list_run.append((i,l))
            else:
                list_run.append((i,l))


if coorde[0] == '2':
    coord_pix_temp=np.loadtxt(run_file_folder+'pix_coord.txt')
    run_range_x_max=int(coord_pix_temp[:,1].max())+1
    run_range_x_min=int(coord_pix_temp[:,1].min())
    run_range_y_max=int(coord_pix_temp[:,0].max())+1
    run_range_y_min=int(coord_pix_temp[:,0].min())
    list_run=[]
    for i in range(len(coord_pix_temp)):
        list_run.append((int(coord_pix_temp[i,1]),int(coord_pix_temp[i,0])))












if len(list_run) ==0:
    print('No valid data!!! Please check your initiallization file or data')



if output_opt==4 and int(n_cycles)>0:
    rcs=call('mkdir initiallization_folder', shell=True)
    os.chdir('initiallization_folder')
    call('cp '+run_file_folder+'*.* '+sir_location+'initiallization_folder/', shell=True)
    call('cp '+run_file_folder+'ASPLUND '+sir_location+'initiallization_folder/', shell=True)
    call('cp '+run_file_folder+'LINES_NLTE '+sir_location+'initiallization_folder/', shell=True)
    call('cp '+run_file_folder+'/atmos/initat.mod '+sir_location+'initiallization_folder/', shell=True)
    convert_profile(nindexa,instindex,core_pos,inwave,fiwave,wavelist,dataa,run_range_x_min,run_range_y_min)

    create_coor_file(disk_pos,1,1)
    try:
        run_desire=subprocess.Popen('echo inver.drtrol | '+sir_location+'desire*.x', shell = True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = run_desire.communicate()
        spect_file_name=glob.glob("*.asc")[0]
        temp_file_spect=open(spect_file_name)
        wave_leng_number=temp_file_spect.readline()
        wave_leng_number=int(wave_leng_number.replace("\n", ""))
        rh_spect_base = multiprocessing.Array(ctypes.c_double, (run_range_x_max-run_range_x_min)*(run_range_y_max-run_range_y_min)*wave_leng_number*5)
        rh_spect = np.ctypeslib.as_array(rh_spect_base.get_obj())
        rh_spect = rh_spect.reshape((5,wave_leng_number,run_range_x_max-run_range_x_min,run_range_y_max-run_range_y_min))
        os.chdir(sir_location)
        call('rm -r initiallization_folder', shell=True)
    except:
        os.chdir(dir_path)
        file = open(dir_path+'/results/errors/error.txt', 'w')
        file.write(out.decode("utf-8"))
        file.write('\n')
        file.write('----------------------------------------------------------')
        file.write('\n')
        file.write(err.decode("utf-8"))
        file.close()
        os.chdir(sir_location)
        call('rm -r initiallization_folder', shell=True)
        sys.exit('Ups something went wrong!! check error.txt in error folder for more details')




















if n_cycles2==0:
    inv_res_array_per_base = multiprocessing.Array(ctypes.c_double, (run_range_x_max-run_range_x_min)*(run_range_y_max-run_range_y_min)*num_lines_per*6)
    inv_res_array_per = np.ctypeslib.as_array(inv_res_array_per_base.get_obj())
    inv_res_array_per = inv_res_array_per.reshape((6,num_lines_per,run_range_x_max-run_range_x_min,run_range_y_max-run_range_y_min))

if n_cycles2>0:
    per_ori_base = multiprocessing.Array(ctypes.c_double, (run_range_x_max-run_range_x_min)*(run_range_y_max-run_range_y_min)*num_lines_per*6)
    per_ori = np.ctypeslib.as_array(per_ori_base.get_obj())
    per_ori = per_ori.reshape((6,num_lines_per,run_range_x_max-run_range_x_min,run_range_y_max-run_range_y_min))

    if clustering.isnumeric():
        atmcl_base = multiprocessing.Array(ctypes.c_double, (run_range_x_max-run_range_x_min)*(run_range_y_max-run_range_y_min))
        atmcl = np.ctypeslib.as_array(atmcl_base.get_obj())
        atmcl = atmcl.reshape((run_range_x_max-run_range_x_min,run_range_y_max-run_range_y_min))

    if output_opt==1 or output_opt==4 or output_opt==2:

        if nlte_f<10:
            fullp_chi2_base = multiprocessing.Array(ctypes.c_double, (run_range_x_max-run_range_x_min)*(run_range_y_max-run_range_y_min)*2)
            fullp_chi2 = np.ctypeslib.as_array(fullp_chi2_base.get_obj())
            fullp_chi2 = fullp_chi2.reshape((2,run_range_x_max-run_range_x_min,run_range_y_max-run_range_y_min))

        if nlte_f>=10:
            fullp_chi2_base = multiprocessing.Array(ctypes.c_double, (run_range_x_max-run_range_x_min)*(run_range_y_max-run_range_y_min))
            fullp_chi2 = np.ctypeslib.as_array(fullp_chi2_base.get_obj())
            fullp_chi2 = fullp_chi2.reshape((run_range_x_max-run_range_x_min,run_range_y_max-run_range_y_min))

        inv_res_array_per_base = multiprocessing.Array(ctypes.c_double, (run_range_x_max-run_range_x_min)*(run_range_y_max-run_range_y_min)*num_lines_per*6)
        inv_res_array_per = np.ctypeslib.as_array(inv_res_array_per_base.get_obj())
        inv_res_array_per = inv_res_array_per.reshape((6,num_lines_per,run_range_x_max-run_range_x_min,run_range_y_max-run_range_y_min))

        inv_res_array_mod_base= multiprocessing.Array(ctypes.c_double, (run_range_x_max-run_range_x_min)*(run_range_y_max-run_range_y_min)*num_lines_mod*11)
        inv_res_array_mod= np.ctypeslib.as_array(inv_res_array_mod_base.get_obj())
        inv_res_array_mod= inv_res_array_mod.reshape(11,num_lines_mod,run_range_x_max-run_range_x_min,run_range_y_max-run_range_y_min)
    if output_opt==2:
        inv_error_mod_base= multiprocessing.Array(ctypes.c_double, (run_range_x_max-run_range_x_min)*(run_range_y_max-run_range_y_min)*num_lines_mod*11)
        inv_error_mod= np.ctypeslib.as_array(inv_error_mod_base.get_obj())
        inv_error_mod= inv_error_mod.reshape(11,num_lines_mod,run_range_x_max-run_range_x_min,run_range_y_max-run_range_y_min)



    if output_opt==3:
        inv_res_array_per_base = multiprocessing.Array(ctypes.c_double, (run_range_x_max-run_range_x_min)*(run_range_y_max-run_range_y_min)*num_lines_per*6*n_cycles2)
        inv_res_array_per = np.ctypeslib.as_array(inv_res_array_per_base.get_obj())
        inv_res_array_per = inv_res_array_per.reshape((n_cycles2,6,num_lines_per,run_range_x_max-run_range_x_min,run_range_y_max-run_range_y_min))

        inv_res_array_mod_base= multiprocessing.Array(ctypes.c_double, (run_range_x_max-run_range_x_min)*(run_range_y_max-run_range_y_min)*num_lines_mod*11*n_cycles2)
        inv_res_array_mod= np.ctypeslib.as_array(inv_res_array_mod_base.get_obj())
        inv_res_array_mod= inv_res_array_mod.reshape(n_cycles2,11,num_lines_mod,run_range_x_max-run_range_x_min,run_range_y_max-run_range_y_min)

        inv_error_mod_base= multiprocessing.Array(ctypes.c_double, (run_range_x_max-run_range_x_min)*(run_range_y_max-run_range_y_min)*num_lines_mod*11*n_cycles2)
        inv_error_mod= np.ctypeslib.as_array(inv_res_array_mod_base.get_obj())
        inv_error_mod= inv_res_array_mod.reshape(n_cycles2,11,num_lines_mod,run_range_x_max-run_range_x_min,run_range_y_max-run_range_y_min)


        if nlte_f<10:
            fullp_chi2_base = multiprocessing.Array(ctypes.c_double, (run_range_x_max-run_range_x_min)*(run_range_y_max-run_range_y_min)*2*n_cycles2)
            fullp_chi2 = np.ctypeslib.as_array(fullp_chi2_base.get_obj())
            fullp_chi2 = fullp_chi2.reshape((n_cycles2,2,run_range_x_max-run_range_x_min,run_range_y_max-run_range_y_min))

        if nlte_f>=10:
            fullp_chi2_base = multiprocessing.Array(ctypes.c_double, (run_range_x_max-run_range_x_min)*(run_range_y_max-run_range_y_min)*n_cycles2)
            fullp_chi2 = np.ctypeslib.as_array(fullp_chi2_base.get_obj())
            fullp_chi2 = fullp_chi2.reshape((n_cycles2,run_range_x_max-run_range_x_min,run_range_y_max-run_range_y_min))


if n_cycles2==-1:
    num_lines_per=1
    rf_base = multiprocessing.Array(ctypes.c_double, (run_range_x_max-run_range_x_min)*(run_range_y_max-run_range_y_min)*dataa[0].shape[1]*fiwave[0]-inwave[0]+1)
    rf = np.ctypeslib.as_array(rf_base.get_obj())
    rf = rf.reshape((4,fiwave[0]-inwave[0]+1,dataa[0].shape[1],run_range_x_max-run_range_x_min,run_range_y_max-run_range_y_min))












































if len(list_run) < ncores:
    ncores=len(list_run)
print('DONE')
print('-------------------------------------------------------')




try:
    #runs the inversion
    if n_cycles2>0 and (coorde[0] == '1' or coorde[0] == '[' or coorde[0] =='2'):
        print('Start inversion using '+str(ncores)+' cores')
        print('-------------------------------------------------------')

        if ncores>0:
            p = multiprocessing.Pool(ncores)
            create_folder(ncores)
            r=p.map(invert, list_run,chunksize=1)
            shp = subprocess.Popen('rm '+run_file_folder+'aux/*.grid', shell = True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            outs, errs = shp.communicate()
            shp = subprocess.Popen('rm '+run_file_folder+'aux/*.wht', shell = True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            outs, errs = shp.communicate()
        else:
            print('no available cores')
except Exception as e:
    print('Something went wrong!!!!!',e)
    print('Saving already computed arryas and plotting maps...')
    os.chdir(dir_path+'/results/')
    if coorde[0] != '2':
        if n_cycles2==0:
            save_fits(inv_res_array_per,'inv_res_pre.fits')

        if n_cycles2>=1:
            save_fits(np.float32(inv_res_array_mod),'inv_res_mod.fits')
            save_fits(np.float32(inv_res_array_per),'inv_res_pre.fits')
            save_fits(np.float32(per_ori),'per_ori.fits')
            if output_opt==2 or output_opt==3:save_fits(np.float32(inv_error_mod),'errors_mod_atmos.fits')
    os.chdir(sir_location)
    call('rm -rf '+intern_folder_name+'*', shell=True)


#runs the sint
if n_cycles2==0 and (coorde[0] == '1' or coorde[0] == '['):
    print('Start syntesis using '+str(ncores)+' cores')
    print('-------------------------------------------------------')

    if ncores>0:
        p = multiprocessing.Pool(ncores)
        p.map(init_folders,range(ncores),chunksize=1)
        p.map(sint, list_run,chunksize=1)
    else:
        print('no available cores')



if n_cycles2==-1 and (coorde[0] == '1' or coorde[0] == '['):
    print('Start rf using '+str(ncores)+' cores')
    print('-------------------------------------------------------')
    if ncores>0:
        p = multiprocessing.Pool(ncores)
        p.map(rffp, list_run,chunksize=1)
    else:
        print('no available cores')




if n_cycles2>0 and coorde[0] == '0':
    shp = subprocess.Popen('rm -r '+dir_path+'/results/run_example/', shell = True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    outs, errs = shp.communicate()
    call('mkdir '+dir_path+'/results/run_example/', shell=True)
    p = multiprocessing.Pool(1)
    create_folder(1)
    p.map(invert, list_run,chunksize=1)
    shp = subprocess.Popen('rm '+run_file_folder+'aux/*.grid', shell = True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    outs, errs = shp.communicate()
    shp = subprocess.Popen('rm '+run_file_folder+'aux/*.wht', shell = True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    outs, errs = shp.communicate()

'''
if n_cycles2==0 and coorde[0] == '0':


if n_cycles2>-1 and coorde[0] == '0':
'''



print('-------------------------------------------------------')
end= time.time()

if n_cycles2>0:
    print('Inversion done in',datetime.timedelta(seconds=int(end-start)))


if n_cycles2==0:
    print('Syntesis done in',datetime.timedelta(seconds=int(end-start)))
print('-------------------------------------------------------')















print('Saving arryas and plotting maps...')





















#Save arrays to fits files in the selected directory

os.chdir(dir_path+'/results/')
if coorde[0] != '2':
    if n_cycles2==0:
        save_fits(inv_res_array_per,'inv_res_pre.fits')

    if n_cycles2>=1:
        save_fits(np.float32(inv_res_array_mod),'inv_res_mod.fits')
        save_fits(np.float32(inv_res_array_per),'inv_res_pre.fits')
        save_fits(np.float32(per_ori),'per_ori.fits')
        save_fits(np.float32(fullp_chi2),'chi2.fits')
        if output_opt==2 or output_opt==3:save_fits(np.float32(inv_error_mod),'erro_mod_atmos.fits')

if clustering.isnumeric():
    save_fits(np.float32(atmcl),'atmcl.fits')

if coorde[0] == '2' and n_cycles2>=1:
    if os.path.exists('inv_res_mod.fits'):
        atmos_temp_dat=fits.open('inv_res_mod.fits')[0]
        atmos_temp_dat=atmos_temp_dat.data
        fitted_temp_dat=fits.open('inv_res_pre.fits')[0]
        fitted_temp_dat=fitted_temp_dat.data
        ori_per_temp_dat=fits.open('per_ori.fits')[0]
        ori_per_temp_dat=ori_per_temp_dat.data
        fullp_chi2_temp_dat=fits.open('chi2.fits')[0]
        fullp_chi2_temp_dat=fullp_chi2_temp_dat.data
        for i in range(len(coord_pix_temp)):
            atmos_temp_dat[:,:,int(coord_pix_temp[i,1])-run_range_x_min+abs(inv_res_array_mod.shape[2]-atmos_temp_dat.shape[2]),int(coord_pix_temp[i,0])-run_range_y_min+abs(inv_res_array_mod.shape[3]-atmos_temp_dat.shape[3])]=inv_res_array_mod[:,:,int(coord_pix_temp[i,1])-run_range_x_min,int(coord_pix_temp[i,0])-run_range_y_min]
            fitted_temp_dat[:,:,int(coord_pix_temp[i,1])-run_range_x_min+abs(inv_res_array_mod.shape[2]-atmos_temp_dat.shape[2]),int(coord_pix_temp[i,0])-run_range_y_min+abs(inv_res_array_mod.shape[3]-atmos_temp_dat.shape[3])]=inv_res_array_per[:,:,int(coord_pix_temp[i,1])-run_range_x_min,int(coord_pix_temp[i,0])-run_range_y_min]
            fullp_chi2_temp_dat[:,int(coord_pix_temp[i,1])-run_range_x_min+abs(inv_res_array_mod.shape[2]-atmos_temp_dat.shape[2]),int(coord_pix_temp[i,0])-run_range_y_min+abs(inv_res_array_mod.shape[3]-atmos_temp_dat.shape[3])]=fullp_chi2[:,int(coord_pix_temp[i,1])-run_range_x_min,int(coord_pix_temp[i,0])-run_range_y_min]
            ori_per_temp_dat[:,:,int(coord_pix_temp[i,1])-run_range_x_min+abs(inv_res_array_mod.shape[2]-atmos_temp_dat.shape[2]),int(coord_pix_temp[i,0])-run_range_y_min+abs(inv_res_array_mod.shape[3]-atmos_temp_dat.shape[3])]=per_ori[:,:,int(coord_pix_temp[i,1])-run_range_x_min,int(coord_pix_temp[i,0])-run_range_y_min]
        plot_maps(atmos_temp_dat,fitted_temp_dat,ori_per_temp_dat,fullp_chi2_temp_dat,output_opt)
        save_fits(np.float32(atmos_temp_dat),'inv_res_mod.fits')
        save_fits(np.float32(fitted_temp_dat),'inv_res_pre.fits')
        save_fits(np.float32(fullp_chi2_temp_dat),'chi2.fits')
        print('DONE')
    else:
        save_fits(np.float32(inv_res_array_mod),'inv_res_mod.fits')
        save_fits(np.float32(inv_res_array_per),'inv_res_pre.fits')
        save_fits(np.float32(per_ori),'per_ori.fits')
        save_fits(np.float32(fullp_chi2),'chi2.fits')
        plot_maps(inv_res_array_mod,inv_res_array_per,per_ori,fullp_chi2,output_opt)




#plot maps and export to pdf file
try:
    if n_cycles2>=1 and coorde[0] != '2':
        plot_maps(inv_res_array_mod,inv_res_array_per,per_ori,fullp_chi2,output_opt)
        print('DONE')
except Exception as e:
    print('Not possible to plot data.',e)


os.chdir(dir_path+'/results/errors/')
list_error=glob.glob('pixel*.txt')
list_error.sort()
os.chdir(dir_path+'/results/')
if len(list_error)>0:
    file_no_cov = open('no_converged_pixels.txt','w')
    for i in range(len(list_error)):
        qq=list_error[i].split('.')[0].split('_')
        file_no_cov.write(str(qq[1])+'  '+str(qq[2])+'\n')
    file_no_cov.close()



os.chdir(sir_location)
shp = subprocess.Popen('rm -rf '+intern_folder_name+'*', shell = True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
outs, errs = shp.communicate()

#Send email after inversion is done
if len(email_adre[0]) >1:
    me="pythongaf@gmail.com"
    mes=email_adre
    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.starttls()
    server.login(me, "caiih3968")

    msg = "\r\n".join([
  "From: SIR inversion",
  "To: user",
  "Subject: Computation done",
  "",
  "Computation done in "+str(datetime.timedelta(seconds=int(end-start)))
  ])
    server.sendmail(me, mes, msg)
    server.quit()





print('_______________________________________________________')
print('_______________________________________________________')
print('_______________________________________________________')
