The idea behind these scripts is to control the environment that each code needs for running an individual inversion in a pixel, distribute all pixels between different processors, and collect the results afterwards. This will allow the user to easily set up inversions, synthesis and computations of response functions of large data sets, with single and multi-line computations from any instrument or line, in reasonable times. Moreover, it includes options in the SIR and DeSIRe wrappers for choosing different types of atmospheric models initialization:
    
    -one single guess atmosphere for all the pixels
    -predefined clustering criteria and respective model atmospheres
    -start from a previously inverted/computed atmosphere
    -CNN initialization
    -using several initial atmospheres and selection the one that best fits the profiles

In the last option, the user just needs to download/compute the CNN model atmosphere for the instrument and respective observed line and the code will automatically estimate the atmospheric initial condition for each pixel and run the inversion. The software includes a list of pre-computed models. 