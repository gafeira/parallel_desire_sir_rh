Python 3 script to run rh in parallel

--------------------------------------------------------------------------------
Necessary python packages (if not available they can be easily installed by PIP or Anaconda):
	os
	sys
	numpy
	matplotlib
	time
	astropy
	time
	subprocess
	datetime
	multiprocessing
	ctypes
	smtplib
	glob
	helita(https://ita-solar.github.io/helita/install/)

-------------------------------------------------------------------------------
File/folder description:
	"parallel_rh.py"- Main script
	"initialization.txt"- Input parameters file. It contains the information regarding the number of cores, x and y range, etc...
	"run_files"- folder containing all necessary input files

--------------------------------------------------------------------------------
To execute run in python 2 or 3 environment: "python parallel_rh.py"


--------------------------------------------------------------------------------
Input files:
	4D array(x,y,physical parameters,height) in fits file format containing the atmosphere information.
	physical parameters description for cmass: Cmass,Temperature,Ne,V,Vturb,field,gamma,phi
	physical parameters description for log(tau): log(tau),Temperature,Ne,V,Vturb,field,gamma,phi,nh(1),nh(2),nh(3),nh(4),nh(5),np
	physical parameters description for gemetrical height: gemetrical height,Temperature,Ne,V,Vturb,field,gamma,phi,nh(1),nh(2),nh(3),nh(4),nh(5),np

--------------------------------------------------------------------------------
Output files:
	"output.txt" ASCII file containing information about script evolution
	"result/rh_spect.fits"-Fits file containing two HDU(frames). First HDU is the computed spectra for each pixel. Second HDU contains the wave length.