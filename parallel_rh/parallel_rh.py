from __future__ import print_function
import sys
#if sys.version_info[0] < 3:
import os
script_folder=os.path.dirname(os.path.realpath(sys.argv[0]))
os.chdir(script_folder)
import numpy as np
import matplotlib.pyplot as plt
import time
import astropy.io.fits as fits
from subprocess import call
import subprocess
import datetime
import multiprocessing
import ctypes
import smtplib
from os.path import expanduser
import glob
import xdrlib
from helita.sim import rh
import shutil
import pathlib
import copy



plt.switch_backend('agg')
plt.ioff()

print('_______________________________________________________')
print('_______________________________________________________')
print('_______________________________________________________')
print('Reading data and initializing arrays...')







#Define all functions

#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------

def save_fits(array,file_name):
    '''
    Save array to a fits file.

    Parameters
    ----------
    first: array that you want to save
    second : Name of the fits file. You can add the path before name
    '''
    hdu = fits.PrimaryHDU(array)
    hdul = fits.HDUList([hdu])
    hdul.writeto(file_name,overwrite=True)



def save_atmos(dataa,nump,x,y,depth_s_t):
    '''
    Create the .atmos file

    Parameters
    ----------
    first: data x position
    second : number of atmospheric points
    '''
    fatmos=open('model.atmos','w')
    if depth_s_t=='m':
        fatmos.write('atmos.mod\n')
        fatmos.write('*\n')
        fatmos.write('Mass scale\n')
        fatmos.write('   4.437970\n')
        fatmos.write('          '+str(nump)+'\n')
        for i in range(nump):
            fatmos.write('{:>15.5e}'.format(dataa[i,0,x,y])+'{:>15.5e}'.format(dataa[i,1,x,y])+'{:>15.5e}'.format(dataa[i,2,x,y])+'{:>15.5e}'.format(dataa[i,3,x,y])+'{:>15.5e}'.format(dataa[i,4,x,y])+'\n')
        fatmos.close()

    if depth_s_t=='h':
        fatmos.write('atmos.mod\n')
        fatmos.write('*\n')
        fatmos.write('Height\n')
        fatmos.write('   4.437970\n')
        fatmos.write('          '+str(nump)+'\n')
        for i in range(nump):
            fatmos.write('{:>15.5e}'.format(dataa[i,0,x,y])+'{:>15.5e}'.format(dataa[i,1,x,y])+'{:>15.5e}'.format(dataa[i,2,x,y])+'{:>15.5e}'.format(dataa[i,3,x,y])+'{:>15.5e}'.format(dataa[i,4,x,y])+'\n')
        for i in range(nump):
            fatmos.write('{:>15.5e}'.format(dataa[i,8,x,y])+'{:>15.5e}'.format(dataa[i,9,x,y])+'{:>15.5e}'.format(dataa[i,10,x,y])+'{:>15.5e}'.format(dataa[i,11,x,y])+'{:>15.5e}'.format(dataa[i,12,x,y])+'{:>15.5e}'.format(dataa[i,13,x,y])+'\n')

        fatmos.close()

    if depth_s_t=='t':
        fatmos.write('atmos.mod\n')
        fatmos.write('*\n')
        fatmos.write('Tau scale\n')
        fatmos.write('   4.437970\n')
        fatmos.write('       '+str(nump)+'\n')
        for i in range(nump):
            fatmos.write('{:>15.5e}'.format(dataa[i,0,x,y])+'{:>15.5e}'.format(dataa[i,1,x,y])+'{:>15.5e}'.format(dataa[i,2,x,y])+'{:>15.5e}'.format(dataa[i,3,x,y])+'{:>15.5e}'.format(dataa[i,4,x,y])+'\n')
        for i in range(nump):
            fatmos.write('{:>15.5e}'.format(dataa[i,8,x,y])+'{:>15.5e}'.format(dataa[i,9,x,y])+'{:>15.5e}'.format(dataa[i,10,x,y])+'{:>15.5e}'.format(dataa[i,11,x,y])+'{:>15.5e}'.format(dataa[i,12,x,y])+'{:>15.5e}'.format(dataa[i,13,x,y])+'\n')
    if len(dataa[0,:,0,0])>6:
#        for i in range(nump):
#            fatmag.write('{:>15.5e}'.format(dataa[i,5,x,y])+'{:>15.5e}'.format(dataa[i,6,x,y])+'{:>15.5e}'.format(dataa[i,7,x,y])+'\n')
        field_xdr=np.zeros(nump*3)

        for i in range(nump):
            field_xdr[i]=dataa[i,5,x,y]
            field_xdr[i+nump]=dataa[i,6,x,y]
            field_xdr[i+2*nump]=dataa[i,7,x,y]


        pout = xdrlib.Packer()
        pout.pack_farray(atmos_points*3,field_xdr,pout.pack_double)

        ff=open('field.B','wb')
        ff.write(pout.get_buffer())
        ff.close()

def change_keyword(n):
    if n>0:
        file_ke=open('run_files/keyword.input','r')
        Input_file_temp=file_ke.readlines()
        file_ke.close()
        Input_file_kk=[]

        for i in range(len(Input_file_temp)):
            if Input_file_temp[i].find('#')<0:
                 Input_file_kk.append(Input_file_temp[i].replace("\n",''))

        for j in range(len(Input_file_kk)):
            if Input_file_kk[j].find('XDR_ENDIAN') >=0:
                Input_file_kk[j]='XDR_ENDIAN = TRUE'

        for j in range(len(Input_file_kk)):
            if Input_file_kk[j].find('STOKES_INPUT') >=0:
                Input_file_kk[j]='STOKES_INPUT = field.B'

        with open('run_files/keyword.input', 'w') as fk:
            for itemke in Input_file_kk:
                fk.write("%s\n" % itemke)
    return()


def vac_air_wave(wav):
    wav2=wav*wav*1.0
    nwave=wav
    if wav >=200:
        fact = 1.0 + 2.735182e-4 + (1.314182e0 + 2.76249e+4/wav2) / wav2
        fact2 = wav/fact
        return(fact2)
    else:
        return(nwave)

def f(t):
    os.chdir(rh_location)
    start2= time.time()
    k=t[0]
    i=t[1]
    call('mkdir prh_'+str(k)+'_'+str(i), shell=True)
    os.chdir('prh_'+str(k)+'_'+str(i))
    call('cp '+run_file_folder+'*.* '+rh_location+'prh_'+str(k)+'_'+str(i)+'/', shell=True)
    if os.path.exists(run_file_folder+'aux/'):
        call('cp '+run_file_folder+'aux/* '+rh_location+'prh_'+str(k)+'_'+str(i)+'/', shell=True)
    save_atmos(atmos_dat,atmos_points,k,i,depth_s_t)
    run_rh=subprocess.Popen('../rhf1d', shell = True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = run_rh.communicate()
    run_solveray=subprocess.Popen('../solveray', shell = True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = run_solveray.communicate()
    if os.path.exists('spectrum.out'):
        int_out=rh.Rhout()
        if n_p_pol==2:
            rh_spect[0,:,k,i]=int_out.int
        if n_p_pol==5:
            rh_spect[0,:,k,i]=int_out.int
            rh_spect[1,:,k,i]=int_out.ray_stokes_Q
            rh_spect[2,:,k,i]=int_out.ray_stokes_U
            rh_spect[3,:,k,i]=int_out.ray_stokes_V
    if os.path.exists('spectrum.out'):
        message='Converged'
    else:
        message='Not converging'
    print('x-'+'{:>4.0f}'.format(k),'   y-'+'{:>4.0f}'.format(i),'{:>10.1f}'.format(time.time()-start2),'s','{:>10.1f}'.format(((i-run_range_y_min)+1+((k-run_range_x_min)*(run_range_y_max-run_range_y_min*1.0)))/((run_range_x_max-run_range_x_min*1.0)*(run_range_y_max-run_range_y_min))*100),'%      ', message)
    os.chdir(rh_location)
    dellf=subprocess.Popen('rm -r prh_'+str(k)+'_'+str(i), shell = True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = dellf.communicate()
    return()

#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------



















dir_path = os.path.dirname(os.path.realpath(__file__))

error=0


if os.path.exists(dir_path+'/results/'):
    None
else:
    call('mkdir '+dir_path+'/results/', shell=True)

if os.path.exists(dir_path+'/results/errors/'):
    if glob.glob(dir_path+'/results/errors/*.txt'):
        call('rm '+dir_path+'/results/errors/*', shell=True)
else:
    call('mkdir '+dir_path+'/results/errors/', shell=True)


home=expanduser("~")
file_in=open('initialization.input','r')
Input_file_temp=file_in.readlines()
file_in.close()
Input_file=[]

for i in range(len(Input_file_temp)):
    if Input_file_temp[i].find('#')<0:
         Input_file.append(Input_file_temp[i])



init_file_keys=['Ncores=','data=','x_min=','x_max=','y_min=','y_max=','run_files=','RH_l=','Email_adress=','depth_s_t=']
init_file_pos=np.zeros(len(init_file_keys))

for i in range(len(init_file_keys)):
    for j in range(len(Input_file)):
        if Input_file[j].find(init_file_keys[i]) >=0:
            init_file_pos[i]=j


ncores=int(Input_file[int(init_file_pos[0])].replace("\n", "").split('=')[1])
data_path=Input_file[int(init_file_pos[1])].replace("\n", "").split('=')[1].split(',')
run_range_x_min=list(map(int,Input_file[int(init_file_pos[2])].replace("\n", "").split('=')[1].split(',')))[0]
run_range_x_max=list(map(int,Input_file[int(init_file_pos[3])].replace("\n", "").split('=')[1].split(',')))[0]
run_range_y_min=list(map(int,Input_file[int(init_file_pos[4])].replace("\n", "").split('=')[1].split(',')))[0]
run_range_y_max=list(map(int,Input_file[int(init_file_pos[5])].replace("\n", "").split('=')[1].split(',')))[0]
init_files=Input_file[int(init_file_pos[6])].replace("\n", "").split('=')[1].split(',')
rh_location=Input_file[int(init_file_pos[7])].replace("\n", "").split('=')[1].split(',')
email_adre=Input_file[int(init_file_pos[8])].replace("\n", "").split('=')[1].split(',')
depth_s_t=Input_file[int(init_file_pos[9])].replace("\n", "").split('=')[1].split(',')[0].lower()


if len(rh_location[0]) >1:
    rh_location=rh_location[0]
else:
    rh_location=home+'/rh/rhf1d/'

if len(init_files) >1:
    run_file_folder=init_files
else:
    run_file_folder=script_folder+'/run_files/'




change_keyword(2)

file_ke=open(run_file_folder+'keyword.input','r')
Input_file_temp=file_ke.readlines()
file_ke.close()
Input_file_k=[]

for i in range(len(Input_file_temp)):
    if Input_file_temp[i].find('#')<0:
         Input_file_k.append(Input_file_temp[i].replace("\n",'').replace(" ",''))


for j in range(len(Input_file_k)):
    if Input_file_k[j].find('STOKES_MODE') >=0:
        pol_int=Input_file_k[j].split('=')[1].lower()

if pol_int=='no_stokes':
    n_p_pol=2
else:
    n_p_pol=5








temp_dat=fits.open(data_path[0])[0]
atmos_dat=temp_dat.data
shapea=temp_dat.shape

if run_range_x_max == -1:
    run_range_x_max=shapea[2]

if run_range_y_max == -1:
    run_range_y_max=shapea[3]
atmos_points=shapea[0]



if (shapea[1]!=14):
    if (shapea[1]!=8):
        sys.exit("Check input data. Invalid number of physical parameters or height stratification definition on initialization.input file")




start= time.time()

os.chdir(rh_location)




#Clean folder from previous incomplete runs
if len(glob.glob('prh_*')) >1:
    call('rm -r prh_*', shell = True)
else:
    None

if len(glob.glob('initiallization_folder')) >0:
    call('rm -rf initiallization_folder', shell = True)
else:
    None

list_run=[]
for i in range(run_range_x_min,run_range_x_max):
    for l in range(run_range_y_min,run_range_y_max):
        list_run.append((i,l))

call('mkdir initiallization_folder', shell=True)
os.chdir('initiallization_folder')
call('cp '+run_file_folder+'*.* '+rh_location+'initiallization_folder/', shell=True)
if os.path.exists(run_file_folder+'aux/'):
    call('cp '+run_file_folder+'aux/* '+rh_location+'initiallization_folder/', shell=True)
save_atmos(atmos_dat,atmos_points,0,0,depth_s_t)






try:
    run_rh=subprocess.Popen('../rhf1d', shell = True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = run_rh.communicate(input=None)
    run_solveray=subprocess.Popen('../solveray', shell = True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out1, err1 = run_solveray.communicate(input=None)
    if os.path.exists('spectrum.out'):
        int_out=rh.Rhout()
        wave_leng_array=int_out.wave
        wave_leng_number=len(wave_leng_array)

    os.chdir(rh_location)
    call('rm -r initiallization_folder', shell=True)






    rh_spect_base = multiprocessing.Array(ctypes.c_double, (run_range_x_max-run_range_x_min)*(run_range_y_max-run_range_y_min)*wave_leng_number*(n_p_pol-1))
    rh_spect = np.ctypeslib.as_array(rh_spect_base.get_obj())
    rh_spect = rh_spect.reshape((n_p_pol-1,wave_leng_number,run_range_x_max-run_range_x_min,run_range_y_max-run_range_y_min))
except:
    os.chdir(dir_path)
    file = open('results/errors/error.txt', 'w')
    file.write(out.decode("utf-8"))
    file.write(err.decode("utf-8"))
    file.write(out1.decode("utf-8"))
    file.write(err1.decode("utf-8"))
    file.close()
    sys.exit('ERROR!! check error.txt for more details')











#Create the global arrays where the data is stored
os.chdir(rh_location)

call('rm -r initiallization_folder', shell=True)


print('DONE')
print('-------------------------------------------------------')

print('Start computation using '+str(ncores)+' cores')
print('-------------------------------------------------------')
#runs the inversion
if ncores>0:
    p = multiprocessing.Pool(ncores)
else:
    p = multiprocessing.Pool()
p.map(f, list_run,chunksize=1)


print('-------------------------------------------------------')
end= time.time()

print('Computation done in',datetime.timedelta(seconds=int(end-start)))
print('-------------------------------------------------------')

print('Saving arryas and plotting maps...')

#Save arrays to fits flies in the selected directory
os.chdir(dir_path+'/results')
#save_fits(np.float32(rh_spect),'rh_spect.fits')

hdu1 = fits.PrimaryHDU(np.float32(rh_spect))
hdu2 = fits.PrimaryHDU(wave_leng_array)
new_hdul = fits.HDUList([hdu1, hdu2])
new_hdul.writeto("rh_spect.fits",overwrite=True)



print('DONE')


print('_______________________________________________________')
print('_______________________________________________________')
print('_______________________________________________________')







#Send email after inversion is done
if len(email_adre[0]) >1:
    me="pythongaf@gmail.com"
    mes=email_adre
    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.starttls()
    server.login(me, "caiih3968")

    msg = "\r\n".join([
  "From: RH computation",
  "To: user",
  "Subject: Computation done",
  "",
  "Computation done in "+str(datetime.timedelta(seconds=int(end-start)))
  ])
    server.sendmail(me, mes, msg)
    server.quit()
