#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  2 12:00:05 2018

@author: gafeira
"""
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button, RadioButtons
import astropy.io.fits as fits
import os
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.patches as patches
from pylab import *
k=1
new_x=k
j=1
new_x=j
cwd = os.getcwd()





plt.close('all')

plt.switch_backend('QT4Agg') #default on my system
print('Backend: {}'.format(plt.get_backend()))


fig = plt.figure(1)
mng = plt._pylab_helpers.Gcf.figs.get(fig.number, None)
mng.window.showMaximized()
gs = plt.GridSpec(100, 100, hspace=0.2, wspace=0.7)
plt.subplots_adjust(top=0.97,
bottom=0.075,
left=0.02,
right=0.985)



#MAPS
ax1 =fig.add_subplot(gs[0:32,0:24])
ax2 =fig.add_subplot(gs[0:32,24:48])
ax3 =fig.add_subplot(gs[33:64,0:24])
ax4 =fig.add_subplot(gs[33:64,24:48])
ax5 =fig.add_subplot(gs[65:98,24:48])


#PROFILES
ax6 =fig.add_subplot(gs[0:18,53:97])
ax7 =fig.add_subplot(gs[18:36,53:97])
ax8 =fig.add_subplot(gs[36:54,53:97])
ax9 =fig.add_subplot(gs[54:72,53:97])
#ATMOSPHERE
initpos=53
larp=10
gapp=2
ax10 =fig.add_subplot(gs[80:99,initpos:initpos+larp])
ax11 =fig.add_subplot(gs[80:99,initpos+larp+gapp:initpos+larp+gapp+larp])
ax12 =fig.add_subplot(gs[80:99,initpos+larp+gapp+larp+gapp:initpos+larp+gapp+larp+gapp+larp])
ax13 =fig.add_subplot(gs[80:99,initpos+larp+gapp+larp+gapp+larp+gapp:initpos+larp+gapp+larp+gapp+larp+gapp+larp])

postext1x=0.042
postext1y=0.3
fig.patches.extend([plt.Rectangle((0.02,0.085),0.203,0.27,fill=False, color='tab:blue', alpha=1, zorder=1000,transform=fig.transFigure, figure=fig)])




class Indexb(object):
    def R(self, event):
        global new_x
        global new_y
        new_x=new_x+1
        newplots(new_x,new_y)
        plt.gcf().text(postext1x, postext1y, '             ', fontsize=10,bbox={'facecolor':'w', 'alpha':1, 'pad':0 ,'edgecolor':'w'})
        plt.gcf().text(postext1x, postext1y, 'x= '+str(new_x), fontsize=10)

        plt.gcf().text(postext1x, postext1y-0.016, '            ', fontsize=10,bbox={'facecolor':'w', 'alpha':1, 'pad':0,'edgecolor':'w'})
        plt.gcf().text(postext1x, postext1y-0.016, 'y= '+str(new_y), fontsize=10)
        return()

    def L(self, event):
        global new_x
        global new_y
        new_x=new_x+1
        newplots(new_x,new_y)
        plt.gcf().text(postext1x, postext1y, '             ', fontsize=10,bbox={'facecolor':'w', 'alpha':1, 'pad':0 ,'edgecolor':'w'})
        plt.gcf().text(postext1x, postext1y, 'x= '+str(new_x), fontsize=10)

        plt.gcf().text(postext1x, postext1y-0.016, '            ', fontsize=10,bbox={'facecolor':'w', 'alpha':1, 'pad':0,'edgecolor':'w'})
        plt.gcf().text(postext1x, postext1y-0.016, 'y= '+str(new_y), fontsize=10)
        return()


    def U(self, event):
        global new_x
        global new_y
        new_y=new_y+1
        newplots(new_x,new_y)
        plt.gcf().text(postext1x, postext1y, '             ', fontsize=10,bbox={'facecolor':'w', 'alpha':1, 'pad':0 ,'edgecolor':'w'})
        plt.gcf().text(postext1x, postext1y, 'x= '+str(new_x), fontsize=10)

        plt.gcf().text(postext1x, postext1y-0.016, '            ', fontsize=10,bbox={'facecolor':'w', 'alpha':1, 'pad':0,'edgecolor':'w'})
        plt.gcf().text(postext1x, postext1y-0.016, 'y= '+str(new_y), fontsize=10)
        return()

    def D(self, event):
        global new_x
        global new_y
        new_y=new_y-1
        newplots(new_x,new_y)
        plt.gcf().text(postext1x, postext1y, '             ', fontsize=10,bbox={'facecolor':'w', 'alpha':1, 'pad':0 ,'edgecolor':'w'})
        plt.gcf().text(postext1x, postext1y, 'x= '+str(new_x), fontsize=10)

        plt.gcf().text(postext1x, postext1y-0.016, '            ', fontsize=10,bbox={'facecolor':'w', 'alpha':1, 'pad':0,'edgecolor':'w'})
        plt.gcf().text(postext1x, postext1y-0.016, 'y= '+str(new_y), fontsize=10)
        return()

callback = Indexb()
axprev = plt.axes([0.022, 0.28, 0.018, 0.025])
axnext = plt.axes([0.064, 0.28, 0.018, 0.025])
axnup = plt.axes([0.042, 0.321, 0.018, 0.025])
axndo = plt.axes([0.042, 0.24, 0.018, 0.025])

bnext = Button(axnext, 'R')
bnext.on_clicked(callback.R)
bprev = Button(axprev, 'L')
bprev.on_clicked(callback.L)
bup =Button(axnup, 'U')
bup.on_clicked(callback.U)
bdo =Button(axndo, 'D')
bdo.on_clicked(callback.D)













home=cwd
os.chdir(home)

dat=fits.open('inv_res_mod.fits')[0]
dat2=fits.open('inv_res_pre.fits')[0]
dat3=fits.open('per_ori.fits')[0]
datshape=dat2.shape




optical_depth=dat.data[0,:,k,j]
waveleng=dat3.data[1,:,k,j]
min_waveleng=min(waveleng)
max_waveleng=max(waveleng)

op_dep_pos=np.where(optical_depth ==0.0)[0]
intensity=dat3.data[2,0,:,:]
magneticfield=dat.data[4,op_dep_pos[0],:,:]
intensity_fitted=dat2.data[2,0,:,:]
#intensity_fitted[intensity_fitted==0] = np.nan

temperature=dat.data[1,op_dep_pos[0],:,:]
temperature[temperature==0] = np.nan

temprma=np.nanmean(temperature)+3*np.nanstd(temperature)
temprmi=np.nanmean(temperature)-3*np.nanstd(temperature)
if temprma > np.max(temperature):
    temprma=np.max(temperature)

if temprmi < np.min(temperature):
    temprmi=np.min(temperature)


velocity=dat.data[5,op_dep_pos[0],:,:]/100000.







p_int=ax1.imshow(intensity.transpose(),origin='lower',cmap='gray',aspect='auto',vmin=intensity.min(),vmax=intensity.max(), extent=[0,datshape[2],0,datshape[3]])

#fig.colorbar(p_int, ax=ax1)
ax1.xaxis.set_visible(False)

p_fited_int=ax3.imshow(intensity_fitted.transpose(),origin='lower',cmap='gray',aspect='auto',vmin=intensity.min(),vmax=intensity.max(), extent=[0,datshape[2],0,datshape[3]])
ax2.yaxis.set_visible(False)
ax2.xaxis.set_visible(False)

#fig.colorbar(p_fited_int, ax=ax2)




p_temp=ax2.imshow(temperature.transpose(),origin='lower',cmap='hot',aspect='auto', extent=[0,datshape[2],0,datshape[3]],vmin=temprmi,vmax=temprma)


dividert = make_axes_locatable(ax2)
cax2 = dividert.append_axes("right", size="3%")
fig.colorbar(p_temp, cax=cax2,format="%i")
ax2.xaxis.set_visible(False)




p_vel=ax4.imshow(velocity.transpose(),origin='lower',cmap='bwr',aspect='auto', extent=[0,datshape[2],0,datshape[3]])
dividerm = make_axes_locatable(ax4)
cax4 = dividerm.append_axes("right", size="3%")
fig.colorbar(p_vel, cax=cax4,format="%.1f")
ax4.yaxis.set_visible(False)
ax4.xaxis.set_visible(False)


p_mag=ax5.imshow(magneticfield.transpose(),origin='lower',cmap='bone',aspect='auto', extent=[0,datshape[2],0,datshape[3]])

dividerv = make_axes_locatable(ax5)
cax5 = dividerv.append_axes("right", size="3%")
fig.colorbar(p_mag, cax=cax5,format="%i")


























ob_lam=dat2.data[1,:,k,j]
ob_profile=dat3.data[2,:,k,j]
ob_profile[ob_profile<-1]=np.nan
fitted_profile=dat2.data[2,:,k,j]
inter=dat3.data[2,:,k,j]




max_int= np.maximum(ob_profile.max(),fitted_profile.max())
min_int= np.minimum(min(fitted_profile/max(fitted_profile)),min(ob_profile/max(fitted_profile)))

ax6.axis([min_waveleng,max_waveleng,min_int,max_int])




p_I_p_1, =ax6.plot(ob_lam[:],fitted_profile[:],label='Fitted',color='tab:blue')
p_I_p_2, =ax6.plot(ob_lam[:],ob_profile[:],'o',label='Observed',color='tab:orange',markersize=5)
#p_I_p_3, =ax6.plot(0,ob_profile[1],'s',color='tab:orange')
#p_I_p_4, =ax6.plot(0,fitted_profile[1],'o',color='tab:blue')
ax6.legend(bbox_to_anchor=(0.87, 0.96), loc=2, borderaxespad=0.)
ax6.set_ylabel('I/Ic')
ax6.yaxis.tick_right()

#ax7.axis([min_waveleng,max_waveleng])

p_Q_p_2, =ax7.plot(ob_lam,dat2.data[3,:,k,j])
p_Q_p_1, =ax7.plot(ob_lam,dat3.data[3,:,k,j],'o',markersize=5)
ax7.set_ylabel('Q/Ic')

ax7.yaxis.tick_right()

#ax8.axis([min_waveleng,max_waveleng,-0.04,0.04])


p_U_p_2, =ax8.plot(ob_lam,dat2.data[4,:,k,j])
p_U_p_1, =ax8.plot(ob_lam,dat3.data[4,:,k,j],'o',markersize=5)
ax8.set_ylabel('U/Ic')

ax8.yaxis.tick_right()



#ax9.axis([min_waveleng,max_waveleng,-0.04,0.04])


p_V_p_2, =ax9.plot(ob_lam,dat2.data[5,:,k,j])
p_V_p_1, =ax9.plot(ob_lam,dat3.data[5,:,k,j],'o',markersize=5)
ax9.set_ylabel('V/Ic')

ax9.yaxis.tick_right()



optical_depth_prof=dat.data[0,:,k,j]
magneticfield_prof=dat.data[0,:,k,j]
temperature_prof=dat.data[1,:,k,j]
velocity_prof=dat.data[5,:,k,j]/100000.



ax10.axis([min(optical_depth),max(optical_depth),min(temperature_prof),max(temperature_prof)])
ax11.axis([min(optical_depth),max(optical_depth),min(magneticfield_prof),max(magneticfield_prof)])
ax12.axis([min(optical_depth),max(optical_depth),min(velocity_prof),max(velocity_prof)])
ax13.axis([min(optical_depth),max(optical_depth),0-10,max(dat.data[2,:,k,j])])



temp_prof, =ax10.plot(optical_depth,temperature_prof)
ax10.set_xlabel('Temperature [k]')


mag_prof, =ax11.plot(optical_depth,magneticfield_prof)
ax11.set_xlabel('B [G]')
#plt.title('B')


vel_prof, =ax12.plot(optical_depth,velocity_prof)
ax12.set_xlabel('V LOS [km/s]')
#plt.title('V LOS')


micro_prof, =ax13.plot(optical_depth,dat.data[3,:,k,j]/1e6)
ax13.set_xlabel('eP')
#plt.title('Microturbulence')




#plt.gcf().text(postext1x, postext1y+0.016, 'Ploting pixel:', fontsize=10)
plt.gcf().text(postext1x, postext1y, 'x= '+str(k), fontsize=10)
plt.gcf().text(postext1x, postext1y-0.016, 'y= '+str(j), fontsize=10)





sle_color = 'lightgoldenrodyellow'
ax_op_dep =   plt.axes([0.3, 0.01, 0.15, 0.03] , facecolor=sle_color)
op_dep =    Slider(ax_op_dep   , 'Opt. depth', min(optical_depth), max(optical_depth), valinit=0.,valfmt='%0.1f',valstep=optical_depth[0]-optical_depth[0])

ax_wave =    plt.axes([0.045, 0.01, 0.15, 0.03], facecolor=sle_color)
op_wave =    Slider(ax_wave   , 'Wave len.', min_waveleng, max_waveleng, valinit=min_waveleng,valfmt='%0.0f',valstep=waveleng[1]-waveleng[0])



ax1p=ax1.get_position()
ax1p=ax1p.get_points()

ax2p=ax2.get_position()
ax2p=ax2p.get_points()

ax3p=ax3.get_position()
ax3p=ax3p.get_points()

ax4p=ax4.get_position()
ax4p=ax4p.get_points()

ax5p=ax5.get_position()
ax5p=ax5p.get_points()





def update(val):
    try:
        op_dep_r=round(op_dep.val, 1)
        new_wa=int((op_wave.val-min_waveleng)/(waveleng[1]-waveleng[0]))

        s,=np.where(optical_depth==op_dep_r)
        intensity=dat3.data[2,new_wa,:,:]
        intensity_fitted=dat2.data[2,new_wa,:,:]
        print(op_dep_r,s)
        p_int.set_data(intensity.transpose())
        p_int.set_clim(intensity.min(),intensity.max())
        p_fited_int.set_data(intensity_fitted.transpose())
        p_fited_int.set_clim(intensity.min(),intensity.max())

        temperature=dat.data[1,int(s),:,:]
        p_temp.set_data(temperature.transpose())
        temprma=np.nanmean(temperature)+2*np.nanstd(temperature)
        temprmi=np.nanmean(temperature)-2*np.nanstd(temperature)
        if temprma > np.max(temperature):
            temprma=np.max(temperature)

        if temprmi < np.min(temperature):
            temprmi=np.min(temperature)
        p_temp.set_clim(temprmi,temprma)

        magneticfield=dat.data[4,int(s),:,:]
        p_mag.set_data(magneticfield.transpose())

        velocity=dat.data[5,int(s),:,:]/100000.
        vel_mean=velocity[~np.isnan(velocity)].mean()
        velocity[velocity==0] = np.nan
        p_vel.set_data(velocity.transpose())
        #p_vel.set_clim(vel_mean-abs(vel_mean)*4-1,vel_mean+abs(vel_mean)*4+1)
        fig.canvas.draw_idle()
    except:
        None
op_dep.on_changed(update)
op_wave.on_changed(update)






pressb=0

def newplots(new_x,new_y):
    ob_profile=dat3.data[2,:,new_x,new_y]
    ob_profile[ob_profile<-1]=np.nan
    fitted_profile=dat2.data[2,:,new_x,new_y]
    max_int= max(ob_profile[-1],max(fitted_profile[:]/max(fitted_profile[:])))
    min_int= np.minimum(min(fitted_profile[:]/max(fitted_profile[:])),min(ob_profile[:]/max(fitted_profile[:])))
    ax6.axis([ob_lam[0],ob_lam[-1],min_int,max_int])
    p_I_p_1.set_data(ob_lam[:],fitted_profile[:]/max(fitted_profile[:]))
    p_I_p_2.set_data(ob_lam[:],ob_profile[:]/max(fitted_profile[:]))
    #p_I_p_3.set_data(0,ob_profile[-1])
    #p_I_p_4.set_data(0,fitted_profile[-1])
    ax7.axis([ob_lam[0],ob_lam[-1],np.min([dat3.data[3,:,new_x,new_y].min(),dat2.data[3,:,new_x,new_y].min()]),np.max([dat3.data[3,:,new_x,new_y].max(),dat2.data[3,:,new_x,new_y].max()])])

    p_Q_p_1.set_data(ob_lam,dat3.data[3,:,new_x,new_y])
    p_Q_p_2.set_data(ob_lam,dat2.data[3,:,new_x,new_y])
    ax8.axis([ob_lam[0],ob_lam[-1],np.min([dat3.data[4,:,new_x,new_y].min(),dat2.data[4,:,new_x,new_y].min()]),np.max([dat3.data[4,:,new_x,new_y].max(),dat2.data[4,:,new_x,new_y].max()])])

    p_U_p_1.set_data(ob_lam,dat3.data[4,:,new_x,new_y])
    p_U_p_2.set_data(ob_lam,dat2.data[4,:,new_x,new_y])
    ax9.axis([ob_lam[0],ob_lam[-1],np.min([dat3.data[5,:,new_x,new_y].min(),dat2.data[5,:,new_x,new_y].min()]),np.max([dat3.data[5,:,new_x,new_y].max(),dat2.data[5,:,new_x,new_y].max()])])

    p_V_p_1.set_data(ob_lam,dat3.data[5,:,new_x,new_y])
    p_V_p_2.set_data(ob_lam,dat2.data[5,:,new_x,new_y])

    optical_depth_prof=dat.data[0,:,new_x,new_y]
    magneticfield_prof=dat.data[4,:,new_x,new_y]
    temperature_prof=dat.data[1,:,new_x,new_y]
    velocity_prof=dat.data[5,:,new_x,new_y]/1000000.
    ax10.axis([min(optical_depth),max(optical_depth),min(temperature_prof),max(temperature_prof)])
    ax11.axis([min(optical_depth),max(optical_depth),min(magneticfield_prof),max(magneticfield_prof)])
    ax12.axis([min(optical_depth),max(optical_depth),min(velocity_prof),max(velocity_prof)])
    temp_prof.set_data(optical_depth,temperature_prof)
    mag_prof.set_data(optical_depth,magneticfield_prof)
    vel_prof.set_data(optical_depth,velocity_prof)
    micro_prof.set_data(optical_depth,dat.data[2,:,new_x,new_y])
    fig.canvas.draw_idle()


def onclick(event):
    try:
        cax1p=event.inaxes.get_position()
        cax1p=cax1p.get_points()
        if (np.all(ax1p[0]==cax1p[0]) or np.all(ax2p[0]==cax1p[0]) or np.all(ax3p[0]==cax1p[0]) or np.all(ax4p[0]==cax1p[0]) or np.all(ax5p[0]==cax1p[0])) and pressb==1:
            new_index=event.xdata,event.ydata
            global new_x
            global new_y
            new_x=int(new_index[0])
            new_y=int(new_index[1])
            newplots(new_x,new_y)

            plt.gcf().text(postext1x, postext1y, '             ', fontsize=10,bbox={'facecolor':'w', 'alpha':1, 'pad':0 ,'edgecolor':'w'})
            plt.gcf().text(postext1x, postext1y, 'x= '+str(new_x), fontsize=10)

            plt.gcf().text(postext1x, postext1y-0.016, '            ', fontsize=10,bbox={'facecolor':'w', 'alpha':1, 'pad':0,'edgecolor':'w'})
            plt.gcf().text(postext1x, postext1y-0.016, 'y= '+str(new_y), fontsize=10)
    except:
        None
        #text.set_text(str(new_x)+' '+str(new_y))

#fig.canvas.mpl_connect('button_press_event', onclick)


def clickon(ev):
    global pressb
    pressb=1
    onclick(ev)

def clickoff(ev):
    global pressb
    pressb=0



fig.canvas.mpl_connect('motion_notify_event', onclick)

fig.canvas.mpl_connect('button_press_event', clickon)
fig.canvas.mpl_connect('button_release_event', clickoff)

#fig.canvas.mpl_connect('button_press_event', onclick)
#fig.canvas.mpl_connect('motion_notify_event', onclick)
#button.on_clicked(onclick)









plt.show()
