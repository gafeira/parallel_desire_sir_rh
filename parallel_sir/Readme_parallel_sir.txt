Python 2.7/3 script to run SIR in parallel
Beta version
December 19th, 2018

--------------------------------------------------------------------------------
Necessary python packages (if not available they can be easily installed by PIP or Anaconda):
sys
os
numpy
matplotlib
time
astropy
subprocess
datetime
multiprocessing
ctypes
smtplib
scipy
glob
warnings

-------------------------------------------------------------------------------
File/folder description:
	"parallel_sir.py"- Main script
	"initiallization.txt"- Input parametres file. It contains the information regarding the number of cores, wavelengths, instruments, etc...
	"/run_files"- folder containing all necessary input files including the .trol file
	"/run_files/atmos"- folder containing initial atmospheric models
	"/run_files/aux"- folder containing auxiliary files to run the computations, like Lines and abundances files
	"/results/aux"- folder containing output files
	"/results/erros"- folder containing information for none converging pixels

--------------------------------------------------------------------------------
To execute run in python envyroment: "python parallel_sir.py"

--------------------------------------------------------------------------------
Input files:
	fits files including the I or IQUV in the format (wavelength,stokes,x,y)
--------------------------------------------------------------------------------
Output files:
	"per_ori.fits"- Observed profiles in SIR format (columns,rows,x,y)
	"inv_res_pre.fits"- Fitted profiles in SIR format (columns,rows,x,y)
	"inv_res_mod.fits"- Fitted atmospheric model in SIR format (columns,rows,x,y)
	"results_sir.pdf"- Pdf file containing the intensity observed, fitted maps, and the velocity and magnetic field maps at different log tau
