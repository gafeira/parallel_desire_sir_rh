import numpy as np
np.random.seed(123)
import os
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv1D, MaxPooling1D
from keras.utils import np_utils
from keras.models import load_model
from keras.callbacks import ModelCheckpoint
from keras.activations import relu
from keras.optimizers import Adam,Adamax
import astropy.io.fits as fits
import sys
stdout = sys.stdout
sys.stdout = open('/dev/null', 'w')
sys.stdout = stdout


from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv1D, MaxPooling1D
from keras.utils import np_utils
from keras.models import load_model
from keras.callbacks import ModelCheckpoint
from keras.activations import relu
from keras.optimizers import Adam,Adamax






import numpy
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
from scipy.optimize import curve_fit

from keras.utils import plot_model

import multiprocessing
import ctypes


temp = fits.open('per_ori.fits')
train_set_spectra2 = temp[0].data
temp = fits.open('inv_res_mod.fits')
train_set_atmos2 = temp[0].data
temp = fits.open('per_ori.fits')
to_fit_spectra2 = temp[0].data


shapespect=train_set_spectra2.shape
print(shapespect)
shapeatmos=train_set_atmos2.shape
shapespectpre=to_fit_spectra2.shape
n_nodes=10
already_trained=2
filepath = 'nn_model_10_38_3.h5'
xi=0
xf=shapespect[2]
yi=0
yf=shapespect[3]
n_batch=64
nodes=np.linspace(0,shapeatmos[1]-1,n_nodes,dtype = int)
print(nodes)
nodes_val=np.arange(1.0,-3.8-0.001,-0.1)
v_norm=30*100000.
t_norm=20000
g_norm=6000


nl=shapespect[1]
total_point_atmos=len(nodes)*3+2


train_set_atmos2[1,:,:,:]=train_set_atmos2[1,:,:,:]/t_norm
train_set_atmos2[4,:,:,:]=train_set_atmos2[4,:,:,:]/g_norm
train_set_atmos2[5,:,:,:]=train_set_atmos2[5,:,:,:]/v_norm+0.5
train_set_atmos2[6,:,:,:]=train_set_atmos2[6,:,:,:]/180.
train_set_atmos2[7,:,:,:]=train_set_atmos2[7,:,:,:]/360.


train_set_spectra=np.zeros(((xf-xi)*(yf-yi),shapespect[1],4))
train_set_atmos=np.zeros(((xf-xi)*(yf-yi),len(nodes)*3+2))


n=0
for i in range(xi,xf):
    for l in range(yi,yf):
        train_set_spectra[n,:,0]=train_set_spectra2[2,:,i,l]
        train_set_spectra[n,:,1]=train_set_spectra2[3,:,i,l]
        train_set_spectra[n,:,2]=train_set_spectra2[4,:,i,l]
        train_set_spectra[n,:,3]=train_set_spectra2[5,:,i,l]*5
        train_set_atmos[n,:len(nodes)]=train_set_atmos2[1,nodes,i,l]
        train_set_atmos[n,len(nodes):len(nodes)+len(nodes)]=train_set_atmos2[4,nodes,i,l]
        train_set_atmos[n,len(nodes)+len(nodes):len(nodes)+len(nodes)+len(nodes)]=train_set_atmos2[5,nodes,i,l]
        train_set_atmos[n,len(nodes)+len(nodes)+len(nodes):len(nodes)+len(nodes)+len(nodes)+1]=train_set_atmos2[6,39,i,l]
        train_set_atmos[n,len(nodes)+len(nodes)+len(nodes)+1:len(nodes)+len(nodes)+len(nodes)+len(nodes)+len(nodes)+1+1]=train_set_atmos2[7,39,i,l]
        n+=1

if already_trained ==2:
    npx=shapespectpre[2]
    npy=shapespectpre[3]
    to_fit_spectra=np.zeros((npx*npy,shapespect[1],4))

    n=0

    for i in range(npx):
        for l in range(npy):
            to_fit_spectra[n,:,0]=to_fit_spectra2[2,:,i,l]
            to_fit_spectra[n,:,1]=to_fit_spectra2[3,:,i,l]
            to_fit_spectra[n,:,2]=to_fit_spectra2[4,:,i,l]
            to_fit_spectra[n,:,3]=to_fit_spectra2[5,:,i,l]*5
            n+=1






"""""""""""""""""""""""""""
Neural network part starts here
"""""""""""""""""""""""""""




if already_trained ==0:
    model=Sequential()
    model.add(Conv1D(n_batch,(7,),activation='relu'))
    model.add(MaxPooling1D(pool_size=(2)))
    model.add(Conv1D(n_batch,(5,),activation='relu'))
    model.add(MaxPooling1D(pool_size=(2)))
    model.add(Conv1D(n_batch,(3,),activation='relu'))
    model.add(Dropout(0.15))
    model.add(Flatten())
    model.add(Dense(nl*4,activation='relu'))
    model.add(Dense(total_point_atmos,activation='linear'))
    model.compile(loss='mean_squared_error',optimizer='adamax')
    model.fit(train_set_spectra,train_set_atmos,batch_size=n_batch,epochs=200,verbose=1,validation_split=0.2)
    model.save(filepath)










if already_trained ==1:
    model = load_model(filepath)
    #Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=True)
    #model.add(Dropout(0.98))

    #model.compile(loss='mean_squared_error',optimizer='adamax')
    checkpoint = ModelCheckpoint(filepath, monitor='loss', verbose=1,save_best_only=True, mode='min')
#    checkpoint = ModelCheckpoint(filepath, monitor='loss', verbose=1, mode='min')

    callbacks_list = [checkpoint]
    model.fit(train_set_spectra,train_set_atmos,batch_size=n_batch,epochs=200,verbose=1,validation_split=0.2, callbacks=callbacks_list)





"""""""""""""""""""""""""""
Neural network part ends here
"""""""""""""""""""""""""""




def func(x, a, b, c):
    return a * np.exp(-b * x) + c

def get_map(t):
    i=t[0]
    l=t[1]
    xnew = np.arange(0, shapeatmos[1], 1)
    nones=np.ones(len(xnew))
    n=t[2]
    atmos_full[0,:,i,l]=nodes_val
    it=interp1d(nodes,to_fit_atmos[n,:len(nodes)]*t_norm, kind='cubic')
    atmos_full[1,:,i,l]=it(xnew)
    point_to_int=to_fit_atmos[n,len(nodes):len(nodes)+len(nodes)]*g_norm
    ig=interp1d(nodes,point_to_int, kind='linear')
    atmos_full[4,:,i,l]=abs(ig(xnew))

    point_to_int=(to_fit_atmos[n,len(nodes)+len(nodes):len(nodes)+len(nodes)+len(nodes)]-0.5)*v_norm
    iv=interp1d(nodes,point_to_int, kind='linear')
    atmos_full[5,:,i,l]=iv(xnew)

    #atmos_full[2,:,i,l]=train_set_atmos2[2,:,i,l]
    atmos_full[3,:,i,l]=1#train_set_atmos2[3,:,i,l]

    #iin=interp1d(nodes,to_fit_atmos[n,len(nodes)+len(nodes)+len(nodes)+len(nodes):len(nodes)+len(nodes)+len(nodes)+len(nodes)+1]*180, kind='linear')
    atmos_full[6,:,i,l]=nones*to_fit_atmos[n,len(nodes)+len(nodes)+len(nodes):len(nodes)+len(nodes)+len(nodes)+1]*180
    #iaz=interp1d(nodes,to_fit_atmos[n,len(nodes)+len(nodes)+len(nodes)+len(nodes)+1:len(nodes)+len(nodes)+len(nodes)+len(nodes)+len(nodes)+len(nodes)+1+1]*360, kind='linear')
    atmos_full[7,:,i,l]=nones*to_fit_atmos[n,len(nodes)+len(nodes)+len(nodes)+1:len(nodes)+len(nodes)+len(nodes)+1+1]*360


ncores=60



if already_trained ==2:
    list_run=[]
    n=0
    for i in range(npx):
        for l in range(npy):
            list_run.append((i,l,n))
            n=n+1
    atmos_full_base= multiprocessing.Array(ctypes.c_double, (11*shapeatmos[1]*npx*npy))
    atmos_full= np.ctypeslib.as_array(atmos_full_base.get_obj())
    atmos_full= atmos_full.reshape(11,shapeatmos[1],npx,npy)
    atmos_full[:,:,:,:]=1

if already_trained ==2:
    model = load_model(filepath)
    to_fit_atmos = np.ones(([shapespect[2]*shapespect[3],total_point_atmos]))
    to_fit_atmos = model.predict(to_fit_spectra)
    p = multiprocessing.Pool(ncores)
    p.map(get_map, list_run,chunksize=1)
    hdu = fits.PrimaryHDU(atmos_full)
    hdu.writeto('atmos_nodes_3.fits',overwrite=True)
